Tips and tricks
===============

* When defining the sites and ctrs, be careful with Python’s numbering, as it starts at 0. If the system's numeration starts at 1 (as in most visualization tools), you need to ensure proper matching.

* In DFTB+, coordinates are read from **molec_file**. However, PBC are not automatically read from this file (although you can still include them). To define PBC, you must either use the keyword **pbc_cell** or specify them explicitly in the DFTB+ input file (either through direct description or using the gen format, which can be the same as the one used for **molec_file**).

* When using VASP, if you specify **potcar_dir**, do not include POTCAR in the input files, as this will result in an error.

* If the results of a calculation are not correctly output (e.g., due to a time limit), it may cause an error. In such cases, all "incorrect" structures should be removed from the directory.