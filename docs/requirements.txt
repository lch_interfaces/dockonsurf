ase>=3.23.0
hdbscan~=0.8.39
matplotlib>=3.9.2
networkx>=3.4.2
numpy>=2.1.3
pycp2k~=0.2.2
pymatgen~=2024.10.3
scikit-learn~=1.5.2
jinja2>=3.1
sphinx~=8.1.3
sphinx_rtd_theme~=3.0.1





