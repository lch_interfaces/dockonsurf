# Sample of DockOnSurf input file. The file format must be an INI file. 
# Input format on https://dockonsurf.readthedocs.io/en/latest/inp_ref_manual.html

[Global]                                # mandatory
project_name = acetic_acid
run_type = full                         # mandatory
code = CP2K                             # mandatory
batch_q_sys = SGE                       # mandatory
max_jobs = 10r 7p 15rq
subm_script = cp2k.sub
pbc_cell = (15.3822 0 0) (-7.6911 13.32138 0) (0 0 26.93789)
special_atoms = (Fe1 Fe) (Fe2 Fe) (O1 O)
potcar_dir = /home/cmarti/potcars

[Isolated]                              # mandatory if isolated
isol_inp_file = tests/cp2k.inp          # mandatory if isolated
molec_file = tests/acetic.xyz           # mandatory if isolated
num_conformers = 2000
pre_opt = MMFF

[Screening]                             # mandatory if screening
screen_inp_file = tests/screen.inp      # mandatory if screening
surf_file = tests/hematite.xyz          # mandatory if screening
use_molec_file = False
select_magns = energy MOI
confs_per_magn = 2
sites = 130 (103 112 118)               # mandatory if screening
surf_norm_vect = auto
adsorption_height = 2
set_angles = internal
sample_points_per_angle = 4
surf_ctrs2 = 127 109                    # Same number of centers as sites
molec_ctrs = 178 (185 187)              # mandatory if screening
molec_ctrs2 = 145 45                    # Same number of centers as molec_ctrs
molec_ctrs3 = 10 9 29 1 1               # Same number of centers as molec_ctrs
max_helic_angle = 120
h_donor = O
h_acceptor = O1
min_coll_height = 1.0
collision_threshold = 1.2
exclude_ads_ctr = False
max_structures = 500

[Refinement]                            # mandatory if refinement
refine_inp_file = tests/refine.inp      # mandatory if refinement
energy_cutoff = 1.0
