This directory contains various example input files for running DockOnSurf with MACE.

To avoid integrating MACE directly into the DockOnSurf environment, 
we use an approach where we generate a script from the input file (run_opt.py). 
This script is then executed in a separate environment where MACE and Torch are installed.

To use this setup in a SLURM queue system, follow these steps:

    1. Load the DockOnSurf environment.
    2. Use a submission script (.sh file in the DOS.inp) to deactivate the DockOnSurf environment and activate the environment with MACE and Torch installed.

To install the MACE environment, we recommend following the procedure outlined in the official documentation:

https://mace-docs.readthedocs.io/en/latest/guide/installation.html