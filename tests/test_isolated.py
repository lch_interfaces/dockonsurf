import unittest
import setup_tests
from src.dockonsurf.isolated import *
from src.dockonsurf.clustering import *
import numpy as np
from rdkit import Chem
from rdkit.Chem import AllChem
import warnings
from sklearn.exceptions import ConvergenceWarning

# Suppress specific sklearn warnings
warnings.filterwarnings("ignore", category=UserWarning, module="sklearn.cluster._affinity_propagation")



mol = Chem.MolFromMolFile('acetic.mol', removeHs=False)
num_confs = 5
AllChem.EmbedMultipleConfs(mol, num_confs)

class TestIsolated(unittest.TestCase):
    def test_remove_c_linked_hs(self):
        h2o = Chem.MolFromSmiles('O')
        h2o = Chem.AddHs(h2o)
        ch4 = Chem.MolFromSmiles('C')
        ch4 = Chem.AddHs(ch4)
        self.assertEqual(len(remove_C_linked_Hs(h2o).GetAtoms()), 3)
        self.assertEqual(len(remove_C_linked_Hs(ch4).GetAtoms()), 1)

    def test_gen_confs(self):
        self.assertEqual(gen_confs(mol, num_confs).GetNumConformers(),
                         num_confs)

    def test_moments_of_inertia(self):
        self.assertIsInstance(get_moments_of_inertia(mol), np.ndarray)
        self.assertEqual(get_moments_of_inertia(mol).shape, (num_confs, 3))

    def test_mmff_opt_confs(self):
        AllChem.EmbedMultipleConfs(mol, num_confs)  # Corrected this line
        self.assertIsInstance(pre_opt_confs(mol)[0], Chem.rdchem.Mol)
        self.assertIsInstance(pre_opt_confs(mol)[1], np.ndarray)
        self.assertIsInstance(pre_opt_confs(mol, max_iters=0), np.ndarray)

    def test_single_atom_molecule(self):
        single_atom = Chem.MolFromSmiles('[H]')
        self.assertEqual(gen_confs(single_atom, 1).GetNumConformers(), 1)

    def test_moments_of_inertia_non_negative(self):
        moi = get_moments_of_inertia(mol)
        self.assertTrue(np.all(moi >= 0), "Moments of inertia should be non-negative.")


class TestClusteringEndToEnd(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        # Load molecule and generate conformers
        cls.mol = Chem.MolFromMolFile('acetic.mol', removeHs=False)
        cls.num_confs = 5
        AllChem.EmbedMultipleConfs(
            cls.mol, numConfs=cls.num_confs, randomSeed=12345, numThreads=0
        )
        cls.mol_list = [
            cls.mol.GetConformer(i).GetOwningMol() for i in range(cls.num_confs)
        ]

    def test_rmsd_matrix(self):
        rmsd_matrix = get_rmsd(self.mol_list)
        # print("RMSD Matrix:\n", rmsd_matrix)  # Debugging

        # Load expected RMSD matrix
        expected_rmsd_matrix = np.load("expected_rmsd_matrix.npy")
        np.testing.assert_array_almost_equal(
            rmsd_matrix,
            expected_rmsd_matrix,
            decimal=6,
            err_msg="RMSD matrix mismatch.",
        )

    def test_clustering_process(self):
        rmsd_matrix = get_rmsd(self.mol_list)

        # Perform clustering
        clusters = get_clusters(np.array([0, 1, 0, 2, 1]))
        exemplars = get_exemplars_affty(rmsd_matrix, clusters)

        # print("Clusters:", clusters)
        # print("Exemplars:", exemplars)

        # Load expected exemplars
        expected_exemplars = np.load("expected_exemplars.npy").tolist()
        self.assertListEqual(
            exemplars, expected_exemplars, "Exemplar indices mismatch."
        )

    def test_full_pipeline(self):
        rmsd_matrix = get_rmsd(self.mol_list)
        clusters = get_clusters(np.array([0, 1, 0, 2, 1]))
        exemplars = get_exemplars_affty(rmsd_matrix, clusters)

        # print("RMSD Matrix:\n", rmsd_matrix)
        # print("Clusters:", clusters)
        # print("Exemplars:", exemplars)

        # Validate exemplars
        expected_exemplars = [0, 1, 3]  # Expected indices
        self.assertListEqual(exemplars, expected_exemplars, "Exemplar mismatch.")


if __name__ == '__main__':
    unittest.main()
