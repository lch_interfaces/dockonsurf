import unittest
import os
import setup_tests
import copy
from rdkit.Chem import Mol, AllChem as Chem
from ase.atoms import Atoms
from ase import data
from src.dockonsurf.formats import adapt_format, confs_to_mol_list, \
    rdkit_mol_to_ase_atoms, collect_confs, add_special_atoms, read_coords_vasp, read_energy_cp2k, read_coords_cp2k

from rdkit import RDLogger

RDLogger.DisableLog('rdApp.warning')

class TestConfsToMolList(unittest.TestCase):
    def setUp(self):
        """Set up the molecule and conformers for testing."""
        # Load the molecule
        self.mol = Chem.MolFromMolFile('acetic.mol', removeHs=False)
        self.num_confs = 5
        Chem.EmbedMultipleConfs(self.mol, numConfs=self.num_confs)

    def test_confs_to_mol_list(self):
        # Check the output type
        self.assertIsInstance(confs_to_mol_list(self.mol), list)

        # Check the total number of conformers
        self.assertEqual(len(confs_to_mol_list(self.mol)), self.num_confs)

        # Check for subset selection
        self.assertEqual(len(confs_to_mol_list(self.mol, [0, 2, 4])), 3)

    def test_conformer_geometry(self):
        # Verify Z-coordinates are valid
        confs_list = confs_to_mol_list(self.mol)
        for conf in confs_list:
            conformer = conf.GetConformer()
            for i in range(conf.GetNumAtoms()):
                pos = conformer.GetAtomPosition(i)
                self.assertTrue(pos.z is not None, "Z-coordinate is missing.")

    def test_unique_conformer_structures(self):
        # Ensure conformers have unique geometries
        confs_list = confs_to_mol_list(self.mol)
        positions_list = [
            [conf.GetConformer().GetAtomPosition(i) for i in range(conf.GetNumAtoms())]
            for conf in confs_list
        ]
        for i in range(len(positions_list)):
            for j in range(i + 1, len(positions_list)):
                self.assertNotEqual(positions_list[i], positions_list[j], "Conformers are identical.")

    def test_preservation_of_atom_count(self):
        # Verify atom counts remain consistent
        confs_list = confs_to_mol_list(self.mol)
        for conf in confs_list:
            self.assertEqual(conf.GetNumAtoms(), self.mol.GetNumAtoms(), "Atom count mismatch.")

class TestMolToAtoms(unittest.TestCase):
    def setUp(self):
        """Set up the RDKit molecule with a single conformer for testing."""
        self.mol = Chem.MolFromMolFile('acetic.mol', removeHs=False)
        Chem.EmbedMolecule(self.mol, Chem.ETKDG())

    def test_rdkit_mol_to_ase_atoms(self):
        # Check the conversion returns an ASE Atoms object
        self.assertIsInstance(rdkit_mol_to_ase_atoms(self.mol), Atoms)

        # Verify that the number of atoms in the ASE Atoms matches the RDKit mol
        atoms = rdkit_mol_to_ase_atoms(self.mol)
        self.assertEqual(self.mol.GetNumAtoms(), len(atoms))

    def test_geometry_consistency(self):
        # Verify coordinates in ASE Atoms match RDKit conformer
        atoms = rdkit_mol_to_ase_atoms(self.mol)
        positions_rdkit = self.mol.GetConformer(0).GetPositions()
        positions_ase = atoms.get_positions()
        for rdkit_pos, ase_pos in zip(positions_rdkit, positions_ase):
            self.assertAlmostEqual(rdkit_pos[0], ase_pos[0], places=6)
            self.assertAlmostEqual(rdkit_pos[1], ase_pos[1], places=6)
            self.assertAlmostEqual(rdkit_pos[2], ase_pos[2], places=6)

    def test_multiple_conformers_warning(self):
        # Add more conformers and check for a warning
        Chem.EmbedMultipleConfs(self.mol, numConfs=2)
        with self.assertLogs(level='WARNING') as cm:
            rdkit_mol_to_ase_atoms(self.mol)
            self.assertIn('A mol object with multiple conformers is parsed', cm.output[0])

class TestAdaptFormat(unittest.TestCase):
    def test_rdkit_mol(self):
        self.assertIsInstance(
            adapt_format('rdkit', 'acetic.mol'),
            Mol)

    def test_rdkit_xyz(self):
        self.assertIsInstance(
            adapt_format('rdkit', 'acetic.xyz'), Mol)

    def test_ase_mol(self):
        self.assertIsInstance(
            adapt_format('ase', 'acetic.mol'),
            Atoms)

    def test_ase_xyz(self):
        self.assertIsInstance(
            adapt_format('ase', 'acetic.xyz'), Atoms)

    def test_not_adeq_req(self):
        self.assertRaises(NotImplementedError, adapt_format, 'hola',
                          'acetic.xyz')
        


    def test_wrong_file_type(self):
        from ase.io.formats import UnknownFileTypeError
        self.assertRaises(UnknownFileTypeError, adapt_format, 'ase', 'good.inp')


class TestCollectConfs(unittest.TestCase):
    # def test_collect_confs_cp2k(self):
    #     # Set up the directory structure
    #     run_type = 'confs_cp2k'
    #     base_dir = os.path.abspath('.')  # Adjust if necessary
    #     conf_dirs = ['conf_1', 'conf_2', 'conf_3']
        
    #     # Expected energies for each conformer (in eV), in the same order as conf_dirs
    #     expected_energies = [
    #         -2068.06522,  # Energy for conf_1
    #         -2068.06550,  # Energy for conf_2
    #         -2068.06580,  # Energy for conf_3
    #     ]
        
    #     # Call collect_confs
    #     atoms_list = collect_confs(conf_dirs, code='cp2k', run_type=run_type, base_dir=base_dir)
        
    #     # Assertions
    #     self.assertEqual(len(atoms_list), len(expected_energies))
    #     for atoms, expected_energy in zip(atoms_list, expected_energies):
    #         self.assertIsInstance(atoms, Atoms)
    #         self.assertIn('energy', atoms.info)
    #         self.assertAlmostEqual(atoms.info['energy'], expected_energy, places=5)


    def test_collect_confs_vasp(self):
        # Set up the directory structure
        run_type = 'confs_VASP'
        conf_dirs = ['conf_0', 'conf_1', 'conf_2']
        
        # Expected energies for each conformer (in eV), in the same order as conf_dirs
        expected_energies = [
            -63.53295846* 27.2113845,  # Energy for conf_1
            -63.54169921* 27.2113845,  # Energy for conf_2
            -63.54219946* 27.2113845,  # Energy for conf_3
        ]
        
        # Call collect_confs
        atoms_list = collect_confs(conf_dirs, code='vasp', run_type=run_type)
        
        # Assertions
        self.assertEqual(len(atoms_list), len(expected_energies))
        for atoms, expected_energy in zip(atoms_list, expected_energies):
            self.assertIsInstance(atoms, Atoms)
            self.assertIn('energy', atoms.info)
            self.assertAlmostEqual(atoms.info['energy'], expected_energy, places=5)



class TestAddSpecialAtoms(unittest.TestCase):
    def setUp(self):
        # Backup the original state of ase.data attributes that will be modified
        self.original_chemical_symbols = copy.deepcopy(data.chemical_symbols)
        self.original_atomic_numbers = copy.deepcopy(data.atomic_numbers)
        self.original_atomic_names = copy.deepcopy(data.atomic_names)
        self.original_atomic_masses_iupac2016 = copy.deepcopy(data.atomic_masses_iupac2016)
        self.original_atomic_masses_common = copy.deepcopy(data.atomic_masses_common)
        self.original_atomic_masses = copy.deepcopy(data.atomic_masses)
        self.original_covalent_radii = copy.deepcopy(data.covalent_radii)
        self.original_reference_states = copy.deepcopy(data.reference_states)

    def tearDown(self):
        # Restore the original state of ase.data attributes
        data.chemical_symbols = self.original_chemical_symbols
        data.atomic_numbers = self.original_atomic_numbers
        data.atomic_names = self.original_atomic_names
        data.atomic_masses_iupac2016 = self.original_atomic_masses_iupac2016
        data.atomic_masses_common = self.original_atomic_masses_common
        data.atomic_masses = self.original_atomic_masses
        data.covalent_radii = self.original_covalent_radii
        data.reference_states = self.original_reference_states

    def test_add_special_atoms(self):
        # Define test input: custom elements and their reference elements
        symbol_pairs = [('Xx', 'H'), ('Yy', 'O')]

        # Assertions before calling the function
        self.assertNotIn('Xx', data.chemical_symbols)
        self.assertNotIn('Yy', data.chemical_symbols)
        self.assertNotIn('Xx', data.atomic_numbers)
        self.assertNotIn('Yy', data.atomic_numbers)

        # Call the function
        add_special_atoms(symbol_pairs)

        # Assertions after calling the function
        # Check if the custom elements are added to chemical_symbols
        self.assertIn('Xx', data.chemical_symbols)
        self.assertIn('Yy', data.chemical_symbols)

        # Check if the atomic_numbers are assigned
        self.assertIn('Xx', data.atomic_numbers)
        self.assertIn('Yy', data.atomic_numbers)

        # Verify that the atomic number is unique and greater than any existing one
        max_atomic_number = max(self.original_atomic_numbers.values())
        self.assertGreater(data.atomic_numbers['Xx'], max_atomic_number)
        self.assertGreater(data.atomic_numbers['Yy'], max_atomic_number)

        # Check that the properties of the custom elements match those of the reference elements
        z_xx = data.atomic_numbers['Xx']
        z_yy = data.atomic_numbers['Yy']
        z_h = data.atomic_numbers['H']
        z_o = data.atomic_numbers['O']

        # Check atomic masses
        self.assertEqual(data.atomic_masses_iupac2016[z_xx], data.atomic_masses_iupac2016[z_h])
        self.assertEqual(data.atomic_masses_iupac2016[z_yy], data.atomic_masses_iupac2016[z_o])

        # Check covalent radii
        self.assertEqual(data.covalent_radii[z_xx], data.covalent_radii[z_h])
        self.assertEqual(data.covalent_radii[z_yy], data.covalent_radii[z_o])

        # Check reference states
        self.assertEqual(data.reference_states[z_xx], data.reference_states[z_h])
        self.assertEqual(data.reference_states[z_yy], data.reference_states[z_o])


class TestReadCoordsVasp(unittest.TestCase):
    def test_read_coords_vasp_with_poscar(self):

            # Load the poscar from the example in confs_VASP
            poscar_path = os.path.join('confs_VASP/conf_0', 'POSCAR')
        
            # Call read_coords_vasp with the path to POSCAR
            atoms = read_coords_vasp(poscar_path)

            # Assertions
            self.assertIsInstance(atoms, Atoms)
            self.assertEqual(len(atoms), 12)  # There are 12 atoms in the POSCAR
            expected_symbols = ['C', 'C', 'C', 'O', 'H', 'H', 'H', 'H', 'H', 'H', 'H', 'H']
            self.assertEqual(atoms.get_chemical_symbols(), expected_symbols)


class TestReadCoordsCp2k(unittest.TestCase):
    def test_read_coords_cp2k_with_restart(self):

            # Load the restart from the example in confs_cp2k
            restart_path = os.path.join('confs_cp2k/conf_0', 'cp2k_isolated-1.restart')
        
            # Call read_coords_cp2k with the path to restart
            atoms = read_coords_cp2k(restart_path)

            # Assertions
            self.assertIsInstance(atoms, Atoms)
            self.assertEqual(len(atoms), 12)  # There are 12 atoms in the restart file
            expected_symbols = ['C', 'C', 'C', 'O', 'H', 'H', 'H', 'H', 'H', 'H', 'H', 'H']
            self.assertEqual(atoms.get_chemical_symbols(), expected_symbols)


class TestReadEnergyCp2k(unittest.TestCase):
    def test_read_energy_cp2k_with_output(self):

            # Load the output from the example in confs_cp2k
            output_path = os.path.join('confs_cp2k/conf_0', 'isolated.out')
        
            # Call read_coords_energy with the path to the output
            energy = read_energy_cp2k(output_path)

            # Assertions
            energy_set = -37.848622572105498 * 27.2113845 
            self.assertEqual(energy, energy_set)  # energy in eV


if __name__ == '__main__':
    unittest.main()
