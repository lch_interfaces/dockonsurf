import unittest
import setup_tests
from unittest.mock import MagicMock, patch
import numpy as np
from ase import Atoms
from ase.build import surface
from ase.spacegroup import crystal
from src.dockonsurf.screening import *
import logging


class TestSelectConfs(unittest.TestCase):
    def setUp(self):
        # Create mock ase.Atoms objects with real, comparable values
        self.confs = []
        for i in range(10):  # Generate 10 mock configurations
            conf = MagicMock(spec=Atoms)
            conf.info = {
                "iso": i,  # Unique ID
                "energy": i * 0.5,  # Energy values
                "moi": None,  # Initialize moi to None
            }
            # Mock `get_moments_of_inertia` to return a numpy array
            conf.get_moments_of_inertia.return_value = np.array([i, i + 1, i + 2])
            self.confs.append(conf)

    def test_select_with_energy(self):
        selected = select_confs(self.confs, magns=["energy"], num_sel=3)
        self.assertEqual(len(selected), 3)
        self.assertEqual(selected[0].info["iso"], 0)  # Lowest energy
        self.assertEqual(selected[-1].info["iso"], 9)  # Highest energy

    def test_select_with_moi(self):
        selected = select_confs(self.confs, magns=["moi"], num_sel=2)
        self.assertEqual(len(selected), 2)
        self.assertIn(self.confs[-1], selected)  # Highest moi
        self.assertIn(self.confs[0], selected)  # Lowest moi

    def test_select_all_when_num_sel_too_large(self):
        selected = select_confs(self.confs, magns=["energy"], num_sel=20)
        self.assertEqual(len(selected), len(self.confs))

    def test_empty_confs(self):
        selected = select_confs([], magns=["energy"], num_sel=3)
        self.assertEqual(selected, [])

    def test_unsupported_magnitudes(self):
        with self.assertRaises(KeyError):  # Or whatever exception is raised
            select_confs(self.confs, magns=["unsupported"], num_sel=3)

    def test_logging_warning_for_large_num_sel(self):
        with self.assertLogs('DockOnSurf', level='WARNING') as log:
            select_confs(self.confs, magns=["energy"], num_sel=20)
            self.assertIn('Number of conformers per magnitude is equal or larger', log.output[0])


class TestGetVectAngle(unittest.TestCase):
    def test_orthogonal_vectors(self):
        # Test 2D orthogonal vectors
        v1 = np.array([1, 0, 0])
        v2 = np.array([0, 1, 0])
        angle = get_vect_angle(v1, v2)
        self.assertAlmostEqual(angle, 90, places=5)

    def test_parallel_vectors(self):
        # Test parallel vectors
        v1 = np.array([1, 0, 0])
        v2 = np.array([2, 0, 0])  # Same direction
        angle = get_vect_angle(v1, v2)
        self.assertAlmostEqual(angle, 0, places=5)

    def test_antiparallel_vectors(self):
        # Test anti-parallel vectors
        v1 = np.array([1, 0, 0])
        v2 = np.array([-1, 0, 0])  # Opposite direction
        angle = get_vect_angle(v1, v2)
        self.assertAlmostEqual(angle, 180, places=5)

    def test_invalid_input(self):
        # Test invalid input (e.g., mismatched dimensions)
        v1 = np.array([1, 0])
        v2 = np.array([1, 0, 0])
        with self.assertRaises(ValueError):
            get_vect_angle(v1, v2)

class TestVectAvg(unittest.TestCase):
    def test_single_vector(self):
        vects = [[1, 2, 3]]
        result = vect_avg(vects)
        expected = np.array([1, 2, 3])
        np.testing.assert_array_equal(result, expected)

    def test_multiple_vectors(self):
        vects = [[1, 2, 3], [4, 5, 6]]
        result = vect_avg(vects)
        expected = np.array([2.5, 3.5, 4.5])
        np.testing.assert_array_almost_equal(result, expected)

class TestGetAtomCoords(unittest.TestCase):
    def test_single_atom_coordinate(self):
        positions = [[0, 0, 0], [1, 1, 1], [2, 2, 2]]
        atoms = Atoms('H3', positions=positions)
        result = get_atom_coords(atoms, center=1)
        expected = np.array([1, 1, 1])  # Position of the second atom
        np.testing.assert_array_equal(result, expected)

    def test_multiple_atoms_mean_coordinate(self):
        positions = [[0, 0, 0], [1, 1, 1], [2, 2, 2]]
        atoms = Atoms('H3', positions=positions)
        result = get_atom_coords(atoms, center=[0, 2])
        expected = np.array([1, 1, 1])  # Mean of positions [0, 0, 0] and [2, 2, 2]
        np.testing.assert_array_almost_equal(result, expected)

    def test_invalid_center(self):
        positions = [[0, 0, 0], [1, 1, 1], [2, 2, 2]]
        atoms = Atoms('H3', positions=positions)
        
        # Invalid index
        with self.assertRaises(ValueError):
            get_atom_coords(atoms, center=10)

        # Invalid type
        with self.assertRaises(ValueError):
            get_atom_coords(atoms, center="invalid")

class TestComputeNormVect(unittest.TestCase):
    @patch("src.dockonsurf.ASANN.coordination_numbers")
    def test_single_atom(self, mock_coordination_numbers):
        # Mock coordination_numbers output for a single atom
        mock_coordination_numbers.return_value = (None, None, None, np.array([[1.0, 0.0, 0.0]]))

        # Create a mock Atoms object
        atoms = MagicMock(spec=Atoms)
        atoms.get_scaled_positions.return_value = np.array([[0, 0, 0]])

        # Test single atom index
        cell = np.eye(3)  # Identity matrix for cell vectors
        result = compute_norm_vect(atoms, idxs=0, cell=cell)
        expected = np.array([-1.0, 0.0, 0.0])  # Normal vector for the mocked data
        np.testing.assert_array_almost_equal(result, expected)

    @patch("src.dockonsurf.ASANN.coordination_numbers")
    def test_multiple_atoms(self, mock_coordination_numbers):
        # Mock coordination_numbers output for multiple atoms
        mock_coordination_numbers.return_value = (
            None,
            None,
            None,
            np.array([[1.0, 0.0, 0.0], [0.0, 1.0, 0.0]]),
        )

        # Create a mock Atoms object
        atoms = MagicMock(spec=Atoms)
        atoms.get_scaled_positions.return_value = np.array([[0, 0, 0], [1, 1, 1]])

        # Test multiple atom indices
        cell = np.eye(3)
        result = compute_norm_vect(atoms, idxs=[0, 1], cell=cell)
        expected = np.array([-0.7071, -0.7071, 0.0])  # Average normalized vector
        np.testing.assert_array_almost_equal(result, expected, decimal=4)

    @patch("src.dockonsurf.ASANN.coordination_numbers")
    def test_degenerate_normals(self, mock_coordination_numbers):
        # Mock coordination_numbers output with degenerate vectors
        mock_coordination_numbers.return_value = (
            None,
            None,
            None,
            np.array([[0.0, 0.0, 0.0], [1.0, 0.0, 0.0]]),
        )

        # Create a mock Atoms object
        atoms = MagicMock(spec=Atoms)
        atoms.get_scaled_positions.return_value = np.array([[0, 0, 0], [1, 1, 1]])

        # Test degenerate vectors
        cell = np.eye(3)
        result = compute_norm_vect(atoms, idxs=[0, 1], cell=cell)
        self.assertTrue(np.isnan(result).any(), "Expected NaN for degenerate vectors")

    def test_invalid_idxs(self):
        # Create a mock Atoms object
        atoms = MagicMock(spec=Atoms)

        # Test invalid idxs type
        with self.assertRaises(ValueError):
            compute_norm_vect(atoms, idxs="invalid", cell=np.eye(3))

        with self.assertRaises(ValueError):
            compute_norm_vect(atoms, idxs=None, cell=np.eye(3))


class TestAlignMolec(unittest.TestCase):
    def test_basic_alignment(self):
        # Create a simple molecule (e.g., H2O)
        positions = [[0, 0, 0], [1, 0, 0], [0, 1, 0]]  # H2O molecule
        molecule = Atoms("HOH", positions=positions)

        # Align molecule with center at origin and reference vector along z-axis
        ctr_coord = [0, 0, 0]
        ref_vect = [0, 0, 1]
        aligned = align_molec(molecule, ctr_coord, ref_vect)

        # Validate the aligned molecule's orientation
        aligned_position = aligned[1].position - aligned[0].position
        aligned_z_projection = np.dot(aligned_position, ref_vect) / np.linalg.norm(aligned_position)
        self.assertAlmostEqual(aligned_z_projection, np.linalg.norm(aligned_position), places=5)

    def test_alignment_with_dummy_atom(self):
        # Create a simple molecule (e.g., H2)
        positions = [[0, 0, 0], [1, 0, 0]]  # H2 molecule
        molecule = Atoms("H2", positions=positions)

        # Align molecule with a center outside the molecule
        ctr_coord = [0.5, 0.5, 0]  # Outside the molecule
        ref_vect = [0, 0, 1]
        aligned = align_molec(molecule, ctr_coord, ref_vect)

        # Validate that the molecule was rotated
        rotated_distance = np.linalg.norm(aligned[0].position - aligned[1].position)
        self.assertAlmostEqual(rotated_distance, 1.0, places=5)

    def test_alignment_with_degenerate_target_vector(self):
        # Create a molecule with symmetric neighbors around the center
        positions = [[-1, 0, 0], [1, 0, 0], [0, 0, 0]]  # Symmetric molecule
        molecule = Atoms("HHO", positions=positions)

        # Align molecule with center at the symmetric center
        ctr_coord = [0, 0, 0]
        ref_vect = [0, 0, 1]
        aligned = align_molec(molecule, ctr_coord, ref_vect)

        # Validate the aligned molecule's orientation
        aligned_position = aligned[1].position - aligned[0].position
        aligned_z_projection = np.dot(aligned_position, ref_vect) / np.linalg.norm(aligned_position)
        self.assertAlmostEqual(aligned_z_projection, 0.0, places=5)  # Expect coplanar alignment



class TestCheckCollision(unittest.TestCase):
    def setUp(self):
        # Create a simple Pt slab
        a = 3.92  # Lattice constant for Pt
        self.slab = surface("Pt", (1, 1, 1), layers=3, vacuum=10.0)
        self.slab_num_atoms = len(self.slab)

        # Create an adsorbate (e.g., H2 molecule)
        self.adsorbate = Atoms("H2", positions=[[0, 0, 15], [0, 0, 16]])

        # Combine slab and adsorbate
        self.slab_molec = self.slab + self.adsorbate

    def test_collision_by_height(self):
        # Vector perpendicular to the surface (z-axis)
        vect = np.array([0, 0, 1])  # Converted to NumPy array
        min_height = 2.0

        # Place adsorbate too close to the slab
        self.adsorbate.translate([0, 0, 12])  # Close to the slab surface

        # Check for collision
        result = check_collision(
            self.slab_molec,
            self.slab_num_atoms,
            min_height,
            vect,
            coll_coeff=1.2,
        )
        self.assertTrue(result, "No collision detected when adsorbate is too close.")

    def test_collision_by_sphere_overlap(self):
        # Vector perpendicular to the surface (z-axis)
        vect = np.array([0, 0, 1])  # Converted to NumPy array
        min_height = False  # Disable height-based check

        # Reduce distance between slab and adsorbate to force sphere overlap
        self.adsorbate.translate([0, 0, 12])

        # Check for collision
        result = check_collision(
            self.slab_molec,
            self.slab_num_atoms,
            min_height,
            vect,
            coll_coeff=1.5,  # Increase collision radius multiplier
        )
        self.assertTrue(result, "No collision detected when spheres overlap.")

    def test_invalid_surface_normal(self):
        # Invalid surface normal vector
        vect = np.array([1, 1, 0])  # Converted to NumPy array
        min_height = 2.0

        with self.assertRaises(ValueError):
            check_collision(
                self.slab_molec,
                self.slab_num_atoms,
                min_height,
                vect,
                coll_coeff=1.2,
            )


class TestDissociation(unittest.TestCase):
    def setUp(self):
        # Create a simple Pt slab
        self.slab = surface("Pt", (1, 1, 1), layers=3, vacuum=10.0)
        self.slab.center(axis=2)

        # Manually create a water molecule (H2O)
        self.molecule = Atoms(
            symbols=["O", "H", "H"],
            positions=[[0, 0, 15], [0.96, 0, 15], [-0.24, 0.92, 15]],
        )  # Place the molecule above the slab

        # Combine the slab and molecule
        self.slab_molec = self.slab + self.molecule
        self.num_atoms_slab = len(self.slab)

    def test_direct_hydrogen_donor(self):
        h_donor = [1]  # Direct hydrogen atom index in molecule
        h_acceptor = ["Pt"]  # Pt atoms in the slab are acceptors

        # Call dissociation
        disso_structs = dissociation(
            self.slab_molec, h_donor, h_acceptor, self.num_atoms_slab
        )

        # Validate the dissociation
        self.assertTrue(len(disso_structs) > 0, "No dissociation occurred.")
        if len(disso_structs) > 0:
            self.assertNotEqual(
                self.slab_molec[h_donor[0]].position.tolist(),
                disso_structs[0][h_donor[0] + self.num_atoms_slab].position.tolist(),
                "Hydrogen did not move after dissociation.",
            )

    def test_neighbor_hydrogen_donor(self):
        h_donor = [0]  # Oxygen atom index in molecule (neighbor of hydrogen)
        h_acceptor = ["Pt"]  # Pt atoms in the slab are acceptors

        # Call dissociation
        disso_structs = dissociation(
            self.slab_molec, h_donor, h_acceptor, self.num_atoms_slab
        )

        # Validate the dissociation
        self.assertTrue(len(disso_structs) > 0, "No dissociation occurred.")
        if len(disso_structs) > 0:
            hydrogen_indices = [1, 2]
            for h_idx in hydrogen_indices:
                self.assertNotEqual(
                    self.slab_molec[h_idx].position.tolist(),
                    disso_structs[0][h_idx + self.num_atoms_slab].position.tolist(),
                    f"Hydrogen {h_idx} did not move after dissociation.",
                )

    def test_no_dissociation(self):
        h_donor = [1]  # Hydrogen atom index in molecule
        h_acceptor = []  # No valid acceptors

        # Call dissociation
        disso_structs = dissociation(
            self.slab_molec, h_donor, h_acceptor, self.num_atoms_slab
        )

        # Validate no dissociation occurred
        self.assertTrue(len(disso_structs) == 0, "Unexpected dissociation occurred.")


class TestAdsEuler(unittest.TestCase):
    def setUp(self):
        self.slab = surface("Pt", (1, 1, 1), layers=3, vacuum=10.0)
        self.slab.center(axis=2)
        self.molecule = Atoms(
            symbols=["O", "H", "H"],
            positions=[[0, 0, 20], [0.96, 0, 20], [-0.24, 0.92, 20]],
        )

        self.ctr_coord = np.array([0, 0, 20])
        self.site_coord = np.array([0, 0, 10])
        self.norm_vect = np.array([0, 0, 1])
        self.num_pts = 3
        self.min_coll_height = 0.5
        self.coll_coeff = 1.2
        self.slab_nghbs = 12
        self.molec_nghbs = 2
        self.h_donor = ["H"]
        self.h_acceptor = ["Pt"]
        self.height = 5.0
        self.excl_atom = False

    @patch('src.dockonsurf.screening.correct_coll', side_effect=lambda *args, **kwargs: (args[0], False))
    @patch('src.dockonsurf.screening.dissociation', side_effect=lambda slab_molec, h_donor, h_acceptor, offset: [])
    def test_generate_orientations(self, mock_dissociation, mock_correct_coll):
        structures = ads_euler(
            orig_molec=self.molecule,
            slab=self.slab,
            ctr_coord=self.ctr_coord,
            site_coord=self.site_coord,
            num_pts=self.num_pts,
            min_coll_height=self.min_coll_height,
            coll_coeff=self.coll_coeff,
            norm_vect=self.norm_vect,
            slab_nghbs=self.slab_nghbs,
            molec_nghbs=self.molec_nghbs,
            h_donor=self.h_donor,
            h_acceptor=self.h_acceptor,
            height=self.height,
            excl_atom=self.excl_atom,
        )

        self.assertGreater(len(structures), 0, "No structures were generated even with collisions mocked out.")

    def test_no_collision_generated(self):
        # Call the ads_euler function
        structures = ads_euler(
            orig_molec=self.molecule,
            slab=self.slab,
            ctr_coord=self.ctr_coord,
            site_coord=self.site_coord,
            num_pts=self.num_pts,
            min_coll_height=self.min_coll_height,
            coll_coeff=self.coll_coeff,
            norm_vect=self.norm_vect,
            slab_nghbs=self.slab_nghbs,
            molec_nghbs=self.molec_nghbs,
            h_donor=False,  # No dissociation
            h_acceptor=False,  # No dissociation
            height=self.height,
            excl_atom=self.excl_atom,
        )

        # Validate that all generated structures have no collisions
        for struct in structures:
            self.assertTrue(
                all(atom.position[2] >= self.site_coord[2] + self.min_coll_height for atom in struct),
                "Collision detected in generated structure."
            )

class TestAdsInternal(unittest.TestCase):
    def setUp(self):
        # Create a simple Pt slab
        self.slab = surface("Pt", (1, 1, 1), layers=3, vacuum=10.0)
        self.slab.center(axis=2)

        # Create a simple molecule (e.g., H2O for testing)
        self.molecule = Atoms(
            symbols=["O", "H", "H"],
            positions=[[0, 0, 20], [0.96, 0, 20], [-0.24, 0.92, 20]],
        )

        # Define adsorption parameters
        self.ctr1_mol = [0]  # Oxygen atom
        self.ctr2_mol = [1]  # First hydrogen atom
        self.ctr3_mol = [2]  # Second hydrogen atom
        self.ctr1_surf = [0]  # First Pt atom
        self.ctr2_surf = [1]  # Second Pt atom
        self.num_pts = 3  # Sample points
        self.min_coll_height = 0.5
        self.coll_coeff = 1.2
        self.norm_vect = np.array([0, 0, 1])  # Normal to the slab
        self.slab_nghbs = 12
        self.molec_nghbs = 2
        self.h_donor = ["H"]
        self.h_acceptor = ["Pt"]
        self.max_hel = 180  # Maximum helicopter angle
        self.height = 5.0
        self.excl_atom = False

    @patch('src.dockonsurf.screening.correct_coll', side_effect=lambda *args, **kwargs: (args[0], False))
    @patch('src.dockonsurf.internal_rotate.internal_rotate', side_effect=lambda *args, **kwargs: None)
    def test_generate_orientations(self, mock_internal_rotate, mock_correct_coll):
        structures = ads_internal(
            orig_molec=self.molecule,
            slab=self.slab,
            ctr1_mol=self.ctr1_mol,
            ctr2_mol=self.ctr2_mol,
            ctr3_mol=self.ctr3_mol,
            ctr1_surf=self.ctr1_surf,
            ctr2_surf=self.ctr2_surf,
            num_pts=self.num_pts,
            min_coll_height=self.min_coll_height,
            coll_coeff=self.coll_coeff,
            norm_vect=self.norm_vect,
            slab_nghbs=self.slab_nghbs,
            molec_nghbs=self.molec_nghbs,
            h_donor=self.h_donor,
            h_acceptor=self.h_acceptor,
            max_hel=self.max_hel,
            height=self.height,
            excl_atom=self.excl_atom,
        )

        # Ensure structures are generated
        self.assertGreater(len(structures), 0, "No structures were generated even with collisions mocked out.")

    def test_no_collision_generated(self):
        # Test without dissociation
        structures = ads_internal(
            orig_molec=self.molecule,
            slab=self.slab,
            ctr1_mol=self.ctr1_mol,
            ctr2_mol=self.ctr2_mol,
            ctr3_mol=self.ctr3_mol,
            ctr1_surf=self.ctr1_surf,
            ctr2_surf=self.ctr2_surf,
            num_pts=self.num_pts,
            min_coll_height=self.min_coll_height,
            coll_coeff=self.coll_coeff,
            norm_vect=self.norm_vect,
            slab_nghbs=self.slab_nghbs,
            molec_nghbs=self.molec_nghbs,
            h_donor=False,  # Disable dissociation
            h_acceptor=False,  # Disable dissociation
            max_hel=self.max_hel,
            height=self.height,
            excl_atom=self.excl_atom,
        )

        # Validate that all generated structures have no collisions
        for struct in structures:
            self.assertTrue(
                all(atom.position[2] >= self.height for atom in struct),
                "Collision detected in generated structure."
            )

class TestAdsorbConfs(unittest.TestCase):
    def setUp(self):
        # Create a simple Pt slab
        self.slab = surface("Pt", (1, 1, 1), layers=3, vacuum=10.0)
        self.slab.center(axis=2)

        # Create a simple molecule (e.g., H2O for testing)
        self.molecule = Atoms(
            symbols=["O", "H", "H"],
            positions=[[0, 0, 20], [0.96, 0, 20], [-0.24, 0.92, 20]],
        )
        self.conf_list = [self.molecule]

        # Define input variables
        self.inp_vars = {
            'molec_ctrs': [[0]],  # Center of the molecule (oxygen)
            'sites': [[0]],  # First Pt atom as adsorption site
            'set_angles': 'euler',  # Use Euler angles for rotation
            'sample_points_per_angle': 3,  # Sample points
            'surf_norm_vect': [0, 0, 1],  # Normal vector as list
            'min_coll_height': 1.0,  # Minimum height for collision check
            'collision_threshold': 1.2,  # Collision threshold
            'exclude_ads_ctr': False,  # Exclude adsorption center
            'h_donor': ["H"],  # Hydrogen donor
            'h_acceptor': ["Pt"],  # Hydrogen acceptor
            'adsorption_height': 5.0,  # Adsorption height
            'pbc_cell': None,  # No periodic boundary condition
        }

    @patch('src.dockonsurf.screening.ads_euler', side_effect=lambda *args, **kwargs: [Atoms(symbols="PtO", positions=[[0, 0, 10], [0, 0, 15]])])
    def test_generate_configurations_euler(self, mock_ads_euler):
        # Ensure the normal vector is converted to a NumPy array
        self.inp_vars['surf_norm_vect'] = np.array(self.inp_vars['surf_norm_vect'])

        structures = adsorb_confs(self.conf_list, self.slab, self.inp_vars)

        # Validate the result
        self.assertGreater(len(structures), 0, "No configurations were generated using Euler angles.")
        self.assertTrue(all(isinstance(struct, Atoms) for struct in structures), "Generated structures are not ASE Atoms objects.")

    @patch('src.dockonsurf.screening.ads_internal', side_effect=lambda *args, **kwargs: [Atoms(symbols="PtO", positions=[[0, 0, 10], [0, 0, 15]])])
    def test_generate_configurations_internal(self, mock_ads_internal):
        # Ensure the normal vector is converted to a NumPy array
        self.inp_vars['surf_norm_vect'] = np.array(self.inp_vars['surf_norm_vect'])

        # Update input variables to use internal angles
        self.inp_vars['set_angles'] = 'internal'
        self.inp_vars['molec_ctrs2'] = [[1]]  # Secondary molecular center
        self.inp_vars['molec_ctrs3'] = [[2]]  # Tertiary molecular center
        self.inp_vars['surf_ctrs2'] = [[1]]  # Secondary surface center
        self.inp_vars['max_helic_angle'] = 180  # Maximum helicopter angle

        structures = adsorb_confs(self.conf_list, self.slab, self.inp_vars)

        # Validate the result
        self.assertGreater(len(structures), 0, "No configurations were generated using internal angles.")
        self.assertTrue(all(isinstance(struct, Atoms) for struct in structures), "Generated structures are not ASE Atoms objects.")

    def test_no_collision_generated(self):
        # Ensure the normal vector is converted to a NumPy array
        self.inp_vars['surf_norm_vect'] = np.array(self.inp_vars['surf_norm_vect'])

        structures = adsorb_confs(self.conf_list, self.slab, self.inp_vars)

        # Validate that all generated structures have no collisions
        for struct in structures:
            self.assertTrue(
                all(atom.position[2] >= self.inp_vars['adsorption_height'] for atom in struct),
                "Collision detected in generated structure."
            )


class TestAddAdsorbate(unittest.TestCase):
    def setUp(self):
        # Create a simple Pt crystal and a surface with a 3x3 cell matrix
        a = 3.92  # Lattice constant for Pt
        pt = crystal("Pt", [(0, 0, 0)], spacegroup=225, cellpar=[a, a, a, 90, 90, 90])
        self.slab = surface(pt, (1, 1, 1), layers=3, vacuum=10.0)
        self.slab.center(vacuum=10.0, axis=2)  # Ensure proper centering with vacuum

    def test_basic_adsorbate(self):
        # Create an adsorbate (e.g., H atom)
        adsorbate = Atoms("H", positions=[[0, 0, 0]])

        # Site coordinates and height
        site_coord = [0, 0, 0]
        ctr_coord = [0, 0, 0]
        height = 1.5

        # Add the adsorbate
        add_adsorbate(self.slab, adsorbate, site_coord, ctr_coord, height)

        # Validate the adsorbate's position
        expected_position = np.array([site_coord[0], site_coord[1], height])
        actual_position = self.slab[-1].position
        np.testing.assert_array_almost_equal(actual_position, expected_position, decimal=3)

    def test_offset_handling(self):
        # Create an adsorbate (e.g., O atom)
        adsorbate = Atoms("O", positions=[[0, 0, 0]])

        # Site coordinates, height, and offset
        site_coord = [0, 0, 0]
        ctr_coord = [0, 0, 0]
        height = 2.0
        offset = [1, 1, 0]  # x, y offsets

        # Add the adsorbate
        add_adsorbate(self.slab, adsorbate, site_coord, ctr_coord, height, offset=offset)

        # Compute the expected position
        offset_position = np.dot(offset[:2], self.slab.get_cell()[:2, :2])
        expected_position = np.array([
            offset_position[0],
            offset_position[1],
            height
        ])
        actual_position = self.slab[-1].position
        np.testing.assert_array_almost_equal(actual_position, expected_position, decimal=3)

    def test_custom_normal_vector(self):
        # Create an adsorbate (e.g., H atom)
        adsorbate = Atoms("H", positions=[[0, 0, 0]])

        # Custom normal vector
        norm_vect = [1, 0, 0]

        # Site coordinates and height
        site_coord = [0, 0, 0]
        ctr_coord = [0, 0, 0]
        height = 1.0

        # Add the adsorbate
        add_adsorbate(self.slab, adsorbate, site_coord, ctr_coord, height, norm_vect=norm_vect)

        # Validate the adsorbate's position
        expected_position = np.array([height, 0, 0])  # Along the x-axis
        actual_position = self.slab[-1].position
        np.testing.assert_array_almost_equal(actual_position, expected_position, decimal=3)

    def test_invalid_site_coord(self):
        # Create an adsorbate (e.g., H atom)
        adsorbate = Atoms("H", positions=[[0, 0, 0]])

        # Invalid site coordinate
        site_coord = "invalid_site"
        ctr_coord = [0, 0, 0]
        height = 1.5

        # Attempt to add adsorbate and expect a TypeError
        with self.assertRaises(TypeError):
            add_adsorbate(self.slab, adsorbate, site_coord, ctr_coord, height)



if __name__ == '__main__':
     unittest.main()
