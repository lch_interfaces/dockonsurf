#!/bin/bash
#$ -S /bin/bash
#$ -cwd
#$ -V

#$ -N H2Otest
#$ -q E5-*,SSD-E5*,CLG*
#$ -pe mpi8* 8
#$ -e stderr
#$ -o stdout
#$ -m aes
#$ -M carles.marti-aliod@ens-lyon.fr 

## Set up the environment
module purge
module load cp2k/5.1_gcc7.2_avx
export OMP_NUM_THREADS=1

## Prepare /scratch to work in it, if possible.
IN_FILES="*.inp* coord.xyz *.wfn *Hessian"
HOMEDIR=$PWD
if [ -d /scratch/E5N ]; then
  WORKDIR=/scratch/E5N/$USER/$JOB_ID
elif [ -d /scratch/X5 ]; then
  WORKDIR=/scratch/X5/$USER/$JOB_ID
elif [ -d /scratch/Chimie ]; then
  WORKDIR=/scratch/Chimie/$USER/$JOB_ID
else 
  WORKDIR=$HOMEDIR/$JOB_ID
fi

mkdir $WORKDIR
cp -r $IN_FILES $WORKDIR
cd $WORKDIR

## Run the job
basename=$(ls *.inp | cut -d "." -f 1)
mpirun -np $NSLOTS cp2k.popt -i $basename.inp -o $basename.out

## Copy files back to home
cp -r * $HOMEDIR
cd $HOMEDIR
rm -r $WORKDIR
