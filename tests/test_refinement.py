import unittest
import setup_tests
from ase import Atoms
from src.dockonsurf.refinement import select_stable_confs

class TestSelectStableConfs(unittest.TestCase):
    def setUp(self):
        # Create sample conformers with different energies
        self.conf1 = Atoms(symbols="H2O", positions=[[0, 0, 0], [1, 0, 0], [0, 1, 0]])
        self.conf1.info['energy'] = -5.0  # Lowest energy

        self.conf2 = Atoms(symbols="H2O", positions=[[0, 0, 0], [1, 0, 0], [0, 1, 1]])
        self.conf2.info['energy'] = -4.8

        self.conf3 = Atoms(symbols="H2O", positions=[[0, 0, 0], [1, 1, 0], [0, 1, 1]])
        self.conf3.info['energy'] = -4.2

        self.conf_list = [self.conf1, self.conf2, self.conf3]

    def test_select_within_cutoff(self):
        # Test with an energy cutoff that includes all conformers
        energy_cutoff = 1.0
        selected_confs = select_stable_confs(self.conf_list, energy_cutoff)
        self.assertEqual(len(selected_confs), 3, "Failed to select all conformers within cutoff.")
        self.assertIn(self.conf1, selected_confs, "Lowest energy conformer not selected.")
        self.assertIn(self.conf2, selected_confs, "Conf2 not selected within cutoff.")
        self.assertIn(self.conf3, selected_confs, "Conf3 not selected within cutoff.")

    def test_select_within_tight_cutoff(self):
        # Test with a tighter energy cutoff
        energy_cutoff = 0.5
        selected_confs = select_stable_confs(self.conf_list, energy_cutoff)
        self.assertEqual(len(selected_confs), 2, "Failed to select conformers within tighter cutoff.")
        self.assertIn(self.conf1, selected_confs, "Lowest energy conformer not selected.")
        self.assertIn(self.conf2, selected_confs, "Conf2 not selected within tighter cutoff.")
        self.assertNotIn(self.conf3, selected_confs, "Conf3 incorrectly selected with tighter cutoff.")

    def test_select_with_zero_cutoff(self):
        # Test with zero energy cutoff
        energy_cutoff = 0.0
        selected_confs = select_stable_confs(self.conf_list, energy_cutoff)
        self.assertEqual(len(selected_confs), 1, "Failed to select only the lowest energy conformer.")
        self.assertIn(self.conf1, selected_confs, "Lowest energy conformer not selected.")
        self.assertNotIn(self.conf2, selected_confs, "Conf2 incorrectly selected with zero cutoff.")
        self.assertNotIn(self.conf3, selected_confs, "Conf3 incorrectly selected with zero cutoff.")

if __name__ == "__main__":
    unittest.main()

