import unittest
import os
import setup_tests
from src.dockonsurf.calculation import *
from shutil import copy, rmtree
from ase import Atoms
from ase.io import read


class TestCheckFinishedCalcsCp2k(unittest.TestCase):
    def test_cp2k_finished_calc(self):

        run_type = 'confs_cp2k'
        code = 'cp2k'
        finished, unfinished = check_finished_calcs(run_type, code)

        # Check that 'conf_0' is identified as finished
        self.assertIn('conf_0', finished)
        self.assertNotIn('conf_0', unfinished)


class TestCheckFinishedCalcsVASP(unittest.TestCase):
    def test_VASP_finished_calc(self):

        run_type = 'confs_VASP'
        code = 'vasp'
        finished, unfinished = check_finished_calcs(run_type, code)

        # Check that 'conf_0' is identified as finished
        self.assertIn('conf_0', finished)
        self.assertNotIn('conf_0', unfinished)



class TestPrepCp2k(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        # Define file paths
        cls.base_dir = os.getcwd()
        cls.confs_dir = os.path.join(cls.base_dir, 'confs_cp2k', 'conf_0')
        cls.inp_file = os.path.join(cls.confs_dir, 'isolated.inp')
        cls.xyz_file = os.path.join(cls.base_dir, 'isopropanol.xyz')
        cls.run_type = 'isolated'
        cls.proj_name = 'test_project'

        # Create the isolated directory in the main directory
        cls.run_type_dir = os.path.join(cls.base_dir, cls.run_type)
        os.makedirs(cls.run_type_dir, exist_ok=True)

        # Load the isopropanol molecule as an Atoms object
        cls.atms_list = [read(cls.xyz_file)]

    @classmethod
    def tearDownClass(cls):
        # Clean up the isolated directory after tests
        if os.path.exists(cls.run_type_dir):
            rmtree(cls.run_type_dir)

    def test_prep_cp2k(self):
        # Call the function
        prep_cp2k(
            inp_file=self.inp_file,
            run_type=self.run_type,
            atms_list=self.atms_list,
            proj_name=self.proj_name
        )

        # Verify the directory structure
        conf_dir = os.path.join(self.run_type_dir, 'conf_0')
        self.assertTrue(os.path.isdir(conf_dir), "conf_0 directory should exist")

        # Check for the presence of the input file
        generated_inp_file = os.path.join(conf_dir, 'isolated.inp')
        self.assertTrue(os.path.isfile(generated_inp_file), "isolated.inp should exist in conf_0")

        # Check for the presence of the coordinate file
        coord_file = os.path.join(conf_dir, 'mol.xyz')
        self.assertTrue(os.path.isfile(coord_file), "mol.xyz should exist in conf_0")

        # Verify the content of cp2k.inp
        with open(generated_inp_file, 'r') as f:
            inp_content = f.read()
        # Match exact case from the file
        self.assertIn('PROJECT_NAME test_project_isolated', inp_content, "Project_name should be updated")
        self.assertIn('COORD_FILE_NAME mol.xyz', inp_content, "Coord_file_name should be updated")



class TestPrepVASP(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        # Define file paths
        cls.base_dir = os.getcwd()
        cls.confs_dir = os.path.join(cls.base_dir, 'confs_VASP', 'conf_0')
        cls.inp_files = [
            os.path.join(cls.confs_dir, "INCAR"),
            os.path.join(cls.confs_dir, "KPOINTS"),
            os.path.join(cls.confs_dir, "POTCAR"),
        ]
        cls.poscar_file = os.path.join(cls.confs_dir, "POSCAR")
        cls.run_type = "isolated"
        cls.proj_name = "test_project"

        # Extract the cell from POSCAR using ASE
        structure = read(cls.poscar_file, format="vasp")
        cls.cell = structure.cell

        # Load the molecule as an Atoms object from POSCAR
        cls.atms_list = [structure]

        # Create a test isolated directory
        cls.run_type_dir = os.path.join(cls.base_dir, cls.run_type)
        os.makedirs(cls.run_type_dir, exist_ok=True)

    @classmethod
    def tearDownClass(cls):
        # Clean up the isolated directory after tests
        if os.path.exists(cls.run_type_dir):
            rmtree(cls.run_type_dir)

    def test_prep_vasp(self):
        # Call the function, passing the extracted cell
        prep_vasp(
            inp_files=self.inp_files,
            run_type=self.run_type,
            atms_list=self.atms_list,
            proj_name=self.proj_name,
            cell=self.cell,  # Pass the cell extracted from POSCAR
            potcar_dir=None,  # POTCAR is included in the input files
        )

        # Verify the directory structure
        conf_dir = os.path.join(self.run_type_dir, "conf_0")
        self.assertTrue(os.path.isdir(conf_dir), "conf_0 directory should exist")

        # Check for the presence of input files
        for file_name in ["INCAR", "KPOINTS", "POTCAR", "POSCAR"]:
            file_path = os.path.join(conf_dir, file_name)
            self.assertTrue(os.path.isfile(file_path), f"{file_name} should exist in conf_0")

        # Verify the content of INCAR
        incar_file = os.path.join(conf_dir, "INCAR")
        with open(incar_file, "r") as f:
            incar_content = f.read()
        self.assertIn(
            f"SYSTEM = {self.proj_name}_{self.run_type}".lower(),
            incar_content.lower(),
            "SYSTEM should be updated in INCAR",
        )

        # Verify POSCAR content matches the input
        poscar_file = os.path.join(conf_dir, "POSCAR")
        generated_structure = read(poscar_file, format="vasp")
        self.assertTrue((generated_structure.cell == self.cell).all(), "Cell should match POSCAR input")


if __name__ == '__main__':
    unittest.main()
