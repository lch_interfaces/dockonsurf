import sys
import os

# Get the absolute path of the parent directory containing 'src'
base_path = os.path.abspath(os.path.join(os.path.dirname(__file__), '../'))
if base_path not in sys.path:
    sys.path.insert(0, base_path)

