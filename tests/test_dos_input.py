import sys
import unittest
import numpy as np
import os
import setup_tests

from src.dockonsurf.dos_input import *
from ase.atoms import Cell


class TestStr2Lst(unittest.TestCase):
    def test_norm_behav(self):
        # Assertions
        self.assertEqual(str2lst('45'), [45])
        self.assertEqual(str2lst('(78, 9)'), [[78, 9]])
        self.assertEqual(str2lst('128,(135 138;141) 87 {45 68}'),
                         [128, 87, [135, 138, 141], [45, 68]])
        self.assertEqual(str2lst('3 4.5', float), [3.0, 4.5])

    def test_exceptions(self):
        # Assertions of error raising
        self.assertRaises(AttributeError, str2lst, 6)
        self.assertRaises(ValueError, str2lst, 'hola')
        self.assertRaises(ValueError, str2lst, '3 ((6 7) 8) 4')
        self.assertRaises(ValueError, str2lst, '3 4.5')

class TestCheckInpFile(unittest.TestCase):

    def test_cp2k_valid_input(self):
        # Test with valid CP2K input file
        self.assertEqual(check_inp_files('cp2k.inp', 'cp2k'), None)

    def test_cp2k_invalid_input(self):
        # Test with invalid CP2K input file
        with self.assertRaises(Exception) as context:
            check_inp_files('wrong.inp', 'cp2k')
        self.assertIn('Invalid CP2K input file', str(context.exception))

    def test_cp2k_missing_file(self):
        # Test with a missing file
        with self.assertRaises(FileNotFoundError) as context:
            check_inp_files('nonexistent.inp', 'cp2k')
        self.assertIn('Input file nonexistent.inp was not found.', str(context.exception))

    def test_vasp_valid_input(self):
        # Test with valid VASP input files
        base_dir = 'confs_VASP/conf_0'
        inp_files = [os.path.join(base_dir, filename) for filename in ['INCAR', 'KPOINTS', 'POTCAR']]
        self.assertEqual(check_inp_files(inp_files, 'vasp'), None)

    def test_vasp_missing_file(self):
        # Test with missing mandatory file (e.g., missing POTCAR)
        base_dir = 'confs_VASP/conf_missing_potcar'
        inp_files = [os.path.join(base_dir, filename) for filename in ['INCAR', 'KPOINTS', 'POTCAR']]
        with self.assertRaises(FileNotFoundError) as context:
            check_inp_files(inp_files, 'vasp')
        self.assertIn('At least one of the mandatory files', str(context.exception))

class TestGoodInput(unittest.TestCase):

    def test_all_good(self):  # TODO debug
        exp_dict = {'isolated': True,
                    'screening': True,
                    'refinement': True,
                    'code': 'cp2k',
                    'batch_q_sys': 'sge',
                    'max_jobs': {"r": 5, "p": 4, "rp": 6},
                    'subm_script': 'cp2k.sub',
                    'pbc_cell': False,
                    'project_name': 'example',
                    'special_atoms': [],
                    'isol_inp_file': 'cp2k.inp',
                    'molec_file': 'acetic.xyz',
                    'num_conformers': 100,
                    'pre_opt': "mmff",
                    'screen_inp_file': 'screen.inp',
                    'surf_file': 'hematite.xyz',
                    'sites': [128, [135, 138, 141]],
                    'molec_ctrs': [178, [185, 187]],
                    'select_magns': ['moi'],
                    'confs_per_magn': 3,
                    'surf_norm_vect': 'auto',
                    'adsorption_height': 2.5,
                    'set_angles': 'internal',
                    'sample_points_per_angle': 4,
                    'collision_threshold': 0.9,
                    'min_coll_height': 1.0,
                    'exclude_ads_ctr': True,
                    'h_donor': ['O'],
                    'max_structures': 50,
                    'use_molec_file': False,
                    'max_helic_angle': 120.0,
                    'molec_ctrs2': [145, 45],
                    'molec_ctrs3': [10, 5],
                    'surf_ctrs2': [24, 5],
                    'h_acceptor': ['O'],
                    'refine_inp_file': 'refine.inp',
                    'energy_cutoff': 1.0,
                    }
        self.assertEqual(exp_dict, read_input('good.inp'))

    def test_run_type(self):
        self.assertEqual(get_run_type(), (True, True, True))

    def test_code(self):
        self.assertEqual(get_code(), 'cp2k')

    def test_batch_q_sys(self):
        self.assertEqual(get_batch_q_sys(), 'sge')

    def test_subm_script(self):
        self.assertEqual(get_subm_script(), 'cp2k.sub')

    def test_project_name(self):
        self.assertEqual(get_project_name(), 'example')

    def test_relaunch_err(self):
        self.assertEqual(get_relaunch_err(), 'geo_not_conv')

    def test_max_jobs(self):
        self.assertEqual(get_max_jobs(), {"r": 5, "p": 4, "rp": 6})

    def test_special_atoms(self):
        self.assertEqual(get_special_atoms(), [])

    def test_isol_inp_file(self):
        self.assertEqual(get_isol_inp_file('cp2k'), 'cp2k.inp')

    def test_molec_file(self):
        self.assertEqual(get_molec_file(), 'acetic.xyz')

    def test_cluster_magns(self):
        self.assertEqual(get_select_magns(), ['moi'])

    def test_num_conformers(self):
        self.assertEqual(get_num_conformers(), 100)

    def test_get_max_structure(self):
        self.assertEqual(get_max_structures(), 50)

    def test_pre_opt(self):
        self.assertEqual(get_pre_opt(), 'mmff')

    def test_screen_inp_file(self):
        self.assertEqual(get_screen_inp_file('cp2k'), 'screen.inp')

    def test_surf_file(self):
        self.assertEqual(get_surf_file(), 'hematite.xyz')

    def test_sites(self):
        self.assertEqual(get_sites(), [128, [135, 138, 141]])

    def test_molec_ctrs(self):
        self.assertEqual(get_molec_ctrs(), [178, [185, 187]])

    def test_molec_ctrs2(self):
        self.assertEqual(get_molec_ctrs2(), [145, 45])
   
    def test_molec_ctrs3(self):
        self.assertEqual(get_molec_ctrs3(), [10, 5])
        
    def test_surf_ctrs2(self):
        self.assertEqual(get_surf_ctrs2(), [24, 5])
    
    def test_surf_norm_vect(self):
        self.assertEqual(get_surf_norm_vect(), 'auto')

    def test_try_disso(self):
        self.assertEqual(get_H_donor([('Fe1', 'Fe'), ('Fe2', 'Fe'),
                                      ('O1', 'O')]), ['O'])
        
    def test_pts_per_angle(self):
        self.assertEqual(get_pts_per_angle(), 4)

    def test_coll_thrsld(self):
        self.assertEqual(get_coll_thrsld(), 0.9)

    def test_set_angle(self):
        self.assertEqual(get_set_angles(), 'internal')
    
    def test_use_molec_file(self):
        self.assertEqual(get_use_molec_file(), False)

    def test_coll_bottom(self):
        self.assertEqual(get_min_coll_height(np.array([1.0, 0.0, 0.0])), 1.0)

    def test_refine_inp_file(self):
        self.assertEqual(get_refine_inp_file('cp2k'), 'refine.inp')

    def test_energy_cutoff(self):
        self.assertEqual(get_energy_cutoff(), 1.0)

    def test_get_max_helic_angle(self):
        self.assertEqual(get_max_helic_angle(), 120.0)

    def test_get_exclude_ads_ctr(self):
        self.assertEqual(get_exclude_ads_ctr(), True)

    def test_adsorption_heigth(self):
        self.assertEqual(get_adsorption_height(), 2.5)

    def test_confs_per_magn(self):
        self.assertEqual(get_confs_per_magn(), 3)

    def test_h_donor(self):
        self.assertEqual(get_H_acceptor([('Fe1', 'Fe'), ('Fe2', 'Fe'),
                                      ('O1', 'O')]), ['O'])

class TestBadInput(unittest.TestCase):
    dos_inp.read('wrong.inp')

    def test_run_type(self):
        self.assertRaises(ValueError, get_run_type)

    def test_code(self):
        self.assertRaises(ValueError, get_code)

    def test_batch_q_sys(self):
        self.assertRaises(ValueError, get_batch_q_sys)

    def test_subm_script(self):
        self.assertRaises(FileNotFoundError, get_subm_script)

    def test_relaunch_err(self):
        self.assertRaises(ValueError, get_relaunch_err)

    def test_max_jobs(self):
        self.assertRaises(ValueError, get_max_jobs)

    def test_special_atoms(self):
        self.assertRaises(ValueError, get_special_atoms)

    def test_isol_inp_file(self):
        self.assertRaises(FileNotFoundError, get_isol_inp_file, "cp2k")

    def test_cluster_magns(self):
        self.assertRaises(ValueError, get_select_magns)

    def test_num_conformers(self):
        self.assertRaises(ValueError, get_num_conformers)

    def test_pre_opt(self):
        self.assertRaises(ValueError, get_pre_opt)

    def test_screen_inp_file(self):
        self.assertRaises(FileNotFoundError, get_screen_inp_file, 'cp2k')

    def test_surf_file(self):
        self.assertRaises(FileNotFoundError, get_surf_file)

    def test_sites(self):
        self.assertRaises(ValueError, get_sites)

    def test_molec_ctrs(self):
        self.assertRaises(ValueError, get_molec_ctrs)

    def test_molec_ctrs2(self):
        self.assertRaises(ValueError, get_molec_ctrs2)

    def test_surf_norm_vect(self):
        self.assertRaises(ValueError, get_surf_norm_vect)

    def test_pts_per_angle(self):
        self.assertRaises(ValueError, get_pts_per_angle)

    def test_coll_thrsld(self):
        self.assertRaises(ValueError, get_coll_thrsld)

    def test_coll_bottom(self):
        self.assertRaises(ValueError, get_min_coll_height, np.array([[1, 0, 0], 
                                                                     [0, 1, 0], 
                                                                     [0, 0,
                                                                      1]]))

    def test_refine_inp_file(self):
        self.assertRaises(FileNotFoundError, get_refine_inp_file, 'cp2k')

    def test_energy_cutoff(self):
        self.assertRaises(ValueError, get_energy_cutoff)

class TestGoodInput_VASP(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        # Ensure the directory exists
        cls.temp_dir = '/tmp/vasp_pbe'
        if not os.path.isdir(cls.temp_dir):
            os.makedirs(cls.temp_dir)

    def test_all_good_VASP(self):
        exp_dict = {
            'isolated': True,
            'screening': False,
            'refinement': False,
            'code': 'vasp',
            'batch_q_sys': 'slurm',
            'subm_script': 'vasp.sh',
            'pbc_cell': Cell([[20.0, 0.0, 0.0], 
                              [0.0, 20.0, 0.0], 
                              [0.0, 0.0, 20.0]]),  # Expected Cell object
            'project_name': 'example_vasp',
            'potcar_dir': '/tmp/vasp_pbe',
            'isol_inp_file': ['INCAR', 'KPOINTS'],
            'molec_file': 'POSCAR',
            'num_conformers': 3,
            'pre_opt': "mmff",
            'max_jobs': {'r': 5, 'p': 4, 'rp': 6},
            'special_atoms': [],
        }

        actual_dict = read_input('good_vasp.inp')

        # Custom comparison for Cell objects numpy ... :'(
        for key, expected_value in exp_dict.items():
            if key == 'pbc_cell':
                np.testing.assert_array_almost_equal(
                    expected_value.array, actual_dict[key].array, decimal=6, 
                    err_msg=f"Mismatch in key '{key}'"
                )
            else:
                self.assertEqual(expected_value, actual_dict[key], f"Mismatch in key '{key}'")

    def test_get_potcar_dir(self):
        expected_dir = '/tmp/vasp_pbe'
        self.assertEqual(get_potcar_dir(), expected_dir)

    def test_get_pbc_cell(self):
        expected_cell = Cell([[20.0, 0.0, 0.0], 
                              [0.0, 20.0, 0.0], 
                              [0.0, 0.0, 20.0]])
        actual_cell = get_pbc_cell()
        np.testing.assert_array_almost_equal(
            expected_cell.array, actual_cell.array, decimal=6,
            err_msg="Mismatch in pbc_cell"
        )

    @classmethod
    def tearDownClass(cls):
        import shutil
        if os.path.isdir(cls.temp_dir):
            shutil.rmtree(cls.temp_dir)

if __name__ == '__main__':
    unittest.main()
