import subprocess

# List of test files to run
test_files = [
    "test_calculation.py",
    "test_dos_input.py",
    "test_formats.py",
    "test_isolated.py",
    "test_refinement.py",
    "test_screening.py",
]

# Run each test file
for test_file in test_files:
    print(f"\nRunning {test_file}...\n{'=' * 50}")
    try:
        # Execute the test file
        result = subprocess.run(
            ["python3", test_file],
            stdout=subprocess.PIPE,
            stderr=subprocess.PIPE,
            text=True
        )
        # Print the output and error streams
        print(result.stdout)
        if result.returncode != 0:
            print(f"Errors in {test_file}:\n{result.stderr}")
        else:
            print(f"{test_file} completed successfully!")
    except Exception as e:
        print(f"Failed to run {test_file}: {e}")

print("\nAll tests finished!")

