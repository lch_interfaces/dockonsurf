"""

*Functions*

check_finished_calcs: Checks if the calculations finished normally or not. For Mace, it needs to include in in the .log file in the laste line "Optimization finished"

prep_cp2k: Prepares the directories to run calculations with CP2K.

prep_vasp: Prepares the directories to run calculations with VASP.

prep_dftbplus : Prepares the directories to run calculations with DFTB+

prep_mace : Prepares the directories to run calculations with MACE

read_dftb_plus_hsd : Reads a DFTB+ HSD file using _parse_genformat_lines and _parse_geometry_dftb_style

get_jobs_status: Returns a list of job status for a list of job ids.

submit_jobs: Submits jobs to a custom queuing system with the provided script

submit_jobs_mace: Submits jobs to a custom queuing system with the provided script for mace (adding a sleep of 15s before each calcuation to let the GPU clean up)

run_calc: Directs calculation run/submission
"""

import os
import logging

logger = logging.getLogger('DockOnSurf')

def check_finished_calcs(run_type, code):
    """Checks if the calculations finished normally or not.

    @param run_type: The type of calculation to check.
    @param code: The code used for the specified job.
    @return finished_calcs: List of calculation directories that have finished
    normally.
    @return unfinished_calcs: List of calculation directories that have finished
    abnormally.
    """
    from glob import glob
    import os
    import ase.io
    from src.dockonsurf.utilities import tail, is_binary
    from src.dockonsurf.utilities import _human_key

    finished_calcs = []
    unfinished_calcs = []
    for conf_dir in sorted(os.listdir(run_type), key=_human_key):
        conf_path = f'{run_type}/{conf_dir}/'
        if not os.path.isdir(conf_path) or 'conf_' not in conf_dir:
            continue
        if code == 'cp2k':
            restart_file_list = glob(f"{conf_path}/*-1.restart")
            if len(restart_file_list) == 0:
                logger.warning(f"No *-1.restart file found on {conf_path}.")
                unfinished_calcs.append(conf_dir)
                continue
            elif len(restart_file_list) > 1:
                warn_msg = f'There is more than one CP2K restart file ' \
                           f'(*-1.restart / in {conf_path}: ' \
                           f'{restart_file_list}. Skipping directory.'
                unfinished_calcs.append(conf_dir)
                logger.warning(warn_msg)
                continue
            elif os.stat(restart_file_list[0]).st_size == 0:
                unfinished_calcs.append(conf_dir)
                logger.warning(f'{restart_file_list[0]} is an empty file.')
                continue
            out_files = []
            for file in os.listdir(conf_path):
                if is_binary(conf_path+file):
                    continue
                with open(conf_path+file, "rb") as out_fh:
                    tail_out_str = tail(out_fh)
                if tail_out_str.count("PROGRAM STOPPED IN") == 1:
                    out_files.append(file)
            if len(out_files) > 1:
                warn_msg = f'There is more than one CP2K output file in ' \
                           f'{conf_path}: {out_files}. Skipping directory.'
                logger.warning(warn_msg)
                unfinished_calcs.append(conf_dir)
            elif len(out_files) == 0:
                warn_msg = f'There is no CP2K output file in {conf_path}. ' \
                           'Skipping directory.'
                logger.warning(warn_msg)
                unfinished_calcs.append(conf_dir)
            else:
                finished_calcs.append(conf_dir)
        elif code == 'vasp':
            out_file_list = glob(f"{conf_path}/OUTCAR")
            if len(out_file_list) == 0:
                unfinished_calcs.append(conf_dir)
            elif len(out_file_list) > 1:
                warn_msg = f'There is more than one file matching the {code} ' \
                           f'pattern for finished calculation (*.out / ' \
                           f'*-1.restart) in {conf_path}: ' \
                           f'{out_file_list}. Skipping directory.'
                logger.warning(warn_msg)
                unfinished_calcs.append(conf_dir)
            else:
                try:
                    ase.io.read(f"{conf_path}/OUTCAR")
                except ValueError:
                    unfinished_calcs.append(conf_dir)
                    continue
                except IndexError:
                    unfinished_calcs.append(conf_dir)
                    continue
                with open(f"{conf_path}/OUTCAR", 'rb') as out_fh:
                    if "General timing and accounting" not in tail(out_fh):
                        unfinished_calcs.append(conf_dir)
                    else:
                        finished_calcs.append(conf_dir)
        elif code == 'dftb+':
            out_file_list = glob(f"{conf_path}/detailed.out")
            if len(out_file_list) == 0:
                unfinished_calcs.append(conf_dir)
                logger.warning(f"No detailed.out file found in {conf_path}.")
            elif len(out_file_list) > 1:
                warn_msg = f'There is more than one DFTB+ detailed.out file in ' \
                           f'{conf_path}: {out_file_list}. Skipping directory.'
                logger.warning(warn_msg)
                unfinished_calcs.append(conf_dir)
            else:
                finished_calcs.append(conf_dir)
                
        elif code == "mace":
            log_file = os.path.join(conf_path, "opt_log.out")  # Use opt_log.out for checking

            if not os.path.isfile(log_file):
                logger.warning(f"No opt_log.out file found in {conf_path}.")
                unfinished_calcs.append(conf_dir)
                continue

            # Read the last non-empty line of opt_log.out
            last_line = ""
            with open(log_file, "r") as log_fh:
                for line in log_fh:
                    if line.strip():  # Ensure it's not an empty line
                        last_line = line.strip()

            # Check if last line indicates successful completion
            if "Optimization finished successfully" in last_line:
                finished_calcs.append(conf_dir)
            else:
                logger.warning(f"MACE calculation in {conf_path} did not finish normally. Last log line: {last_line}")
                unfinished_calcs.append(conf_dir)
        else:
            err_msg = f"Check not implemented for '{code}'."
            logger.error(err_msg)
            raise NotImplementedError(err_msg)
    return finished_calcs, unfinished_calcs
    

def prep_cp2k(inp_file: str, run_type: str, atms_list: list, proj_name: str):
    """Prepares the directories to run calculations with CP2K.

    @param inp_file: CP2K Input file to run the calculations with.
    @param run_type: Type of calculation. 'isolated', 'screening' or
        'refinement'
    @param atms_list: list of ase.Atoms objects to run the calculation of.
    @param proj_name: name of the project
    @return: None
    """
    from shutil import copy
    from pycp2k import CP2K
    from src.dockonsurf.utilities import check_bak
    if not isinstance(inp_file, str):
        err_msg = "'inp_file' must be a string with the path of the CP2K " \
                  "input file."
        logger.error(err_msg)
        raise ValueError(err_msg)
    cp2k = CP2K()
    cp2k.parse(inp_file)
    cp2k.CP2K_INPUT.GLOBAL.Project_name = proj_name+"_"+run_type
    force_eval = cp2k.CP2K_INPUT.FORCE_EVAL_list[0]
    if force_eval.SUBSYS.TOPOLOGY.Coord_file_name is None:
        logger.warning("'COORD_FILE_NAME' not specified on CP2K input. Using\n"
                       "'coord.xyz'. A new CP2K input file with "
                       "the 'COORD_FILE_NAME' variable is created.")
        force_eval.SUBSYS.TOPOLOGY.Coord_file_name = 'coord.xyz'
        check_bak(inp_file.split('/')[-1])
    new_inp_file = inp_file.split('/')[-1]
    cp2k.write_input_file(new_inp_file)

    coord_file = force_eval.SUBSYS.TOPOLOGY.Coord_file_name

    # Creating and setting up directories for every configuration.
    for i, conf in enumerate(atms_list):
        subdir = f'{run_type}/conf_{i}/'
        os.mkdir(subdir)
        copy(new_inp_file, subdir)
        conf.write(subdir + coord_file)


def prep_vasp(inp_files, run_type, atms_list, proj_name, cell, potcar_dir):
    """Prepares the directories to run calculations with VASP.

    @param inp_files: VASP Input files to run the calculations with.
    @param run_type: Type of calculation. 'isolated', 'screening' or
        'refinement'
    @param atms_list: list of ase.Atoms objects to run the calculation of.
    @param proj_name: name of the project.
    @param cell: Cell for the Periodic Boundary Conditions.
    @param potcar_dir: Directory to find POTCARs for each element.
    @return: None
    """
    from shutil import copy
    import os

    import numpy as np
    from pymatgen.io.vasp.inputs import Incar

    if not potcar_dir:
        mand_files = ["INCAR", "KPOINTS", "POTCAR"]
    elif any("POTCAR" in inp_file for inp_file in inp_files):
        mand_files = ["INCAR", "KPOINTS", "POTCAR"]
    else:
        mand_files = ["INCAR", "KPOINTS"]

    # Check that there are many specified files
    if not isinstance(inp_files, list) and all(isinstance(inp_file, str)
                                               for inp_file in inp_files):
        err_msg = "'inp_files' should be a list of file names/paths"
        logger.error(err_msg)
        ValueError(err_msg)
    # Check that all mandatory files are defined
    elif any(not any(mand_file in inp_file.split("/")[-1]
                     for inp_file in inp_files) for mand_file in mand_files):
        err_msg = f"At least one of the mandatory files {mand_files} was " \
                  "not specified."
        logger.error(err_msg)
        raise FileNotFoundError(err_msg)
    # Check that the defined files exist
    elif any(not os.path.isfile(inp_file) for inp_file in inp_files):
        err_msg = f"At least one of the mandatory files {mand_files} was " \
                  "not found."
        logger.error(err_msg)
        raise FileNotFoundError(err_msg)
    incar = ""
    for i, inp_file in enumerate(inp_files):
        file_name = inp_file.split("/")[-1]
        if "INCAR" in file_name:
            incar = Incar.from_file(inp_file)
            incar["SYSTEM"] = proj_name+"_"+run_type

    # Builds the directory hierarchy and copies/creates the relevant files
    for c, conf in enumerate(atms_list):
        subdir = f'{run_type}/conf_{c}/'
        os.mkdir(subdir)
        for inp_file in inp_files:
            file_name = inp_file.split("/")[-1]
            if "INCAR" in file_name:
                incar.write_file(subdir+"INCAR")
            elif "KPOINTS" in file_name and "KPOINTS" != file_name:
                copy(inp_file, subdir+"KPOINTS")
            elif "POTCAR" in file_name and "POTCAR" != file_name:
                copy(inp_file, subdir+"POTCAR")
            else:
                copy(inp_file, subdir)
        if cell is not False and np.linalg.det(cell) != 0.0:
            conf.pbc = True
            conf.cell = cell
            conf.center()
        elif np.linalg.det(conf.cell) == 0:
            err_msg = "Cell is not defined"
            logger.error(err_msg)
            raise ValueError(err_msg)
        conf.write(subdir+"POSCAR", format="vasp")
        if "POTCAR" not in mand_files and potcar_dir:  # TODO make just once
            poscar_fh = open(subdir+"POSCAR", "r")
            grouped_symbols = poscar_fh.readline().split()
            poscar_fh.close()
            for symbol in grouped_symbols:
                potcar_sym_fh = open(f"{potcar_dir}/{symbol}/POTCAR", "r")
                potcar_sym_str = potcar_sym_fh.read()
                potcar_sym_fh.close()
                potcar_fh = open(subdir+"POTCAR", "a")
                potcar_fh.write(potcar_sym_str)
                potcar_fh.close()


def prep_dftbplus(inp_file: str, run_type: str, atms_list: list, proj_name: str, cell=None):
    """
    Prepares the directories to run calculations with DFTB+. Each subdirectory references 'coord.gen'
    in a GenFormat geometry. If 'atms_list' is empty, geometry is parsed from 'inp_file'.
    
    @param inp_file:   Path to the DFTB+ input file.
    @param run_type:   Label for subdirectories (e.g. "isolated", "screening", ...).
    @param atms_list:  List of ASE Atoms; if empty, geometry is taken from 'inp_file'.
    @param proj_name:  Used to unify or insert OutputPrefix in the Driver block.
    @param cell: Cell for the Periodic Boundary Conditions.
    """
    import re
    import numpy as np
    from shutil import copy
    from ase.io import write
    from ase import Atoms
    from src.dockonsurf.formats import read_dftb_plus_hsd
    
    if not os.path.isfile(inp_file):
        logger.error(f"DFTB+ input file '{inp_file}' not found.")
        raise FileNotFoundError(f"DFTB+ input file '{inp_file}' not found.")

    dftb_data = read_dftb_plus_hsd(inp_file)

    if not atms_list:
        if dftb_data.get("atoms") and len(dftb_data["atoms"]) > 0:
            atms_list = [dftb_data["atoms"]]
            logger.info("Using geometry from the original DFTB+ input.")
        else:
            logger.error("No geometry found in input file and no 'atms_list' provided.")
            raise ValueError("No geometry found in input file and no 'atms_list' provided.")

    # Collect non-geometry blocks
    other_blocks = {}
    for k, v in dftb_data.items():
        if k not in ("geometry", "atoms", "Geometry"):
            other_blocks[k] = v

    # Clean up Driver block OutputPrefix lines
    if "Driver" in other_blocks:
        old_block = other_blocks["Driver"]
        block_lines = old_block.splitlines(keepends=True)

        new_lines = []
        in_driver = False
        found_first_prefix = False
        brace_depth = 0

        for line in block_lines:
            if "Driver" in line and "{" in line:
                in_driver = True
                brace_depth += line.count("{") - line.count("}")
                new_lines.append(line)
                continue

            if in_driver:
                brace_depth += line.count("{") - line.count("}")
                if "OutputPrefix" in line:
                    if not found_first_prefix:
                        new_line = re.sub(
                            r'(OutputPrefix\s*=\s*)([^\s#]+)',
                            rf'\1"{proj_name}"',
                            line
                        )
                        new_lines.append(new_line)
                        found_first_prefix = True
                    # skip other OutputPrefix lines
                    continue

                if "}" in line and brace_depth <= 0:
                    if not found_first_prefix:
                        new_lines.append(f'  OutputPrefix = "{proj_name}"\n')
                        found_first_prefix = True
                    new_lines.append(line)
                    in_driver = False
                else:
                    new_lines.append(line)
            else:
                new_lines.append(line)

        other_blocks["Driver"] = "".join(new_lines)

    # Create subdirectories for each configuration
    for i, atoms in enumerate(atms_list):
        conf_dir = os.path.join(run_type, f"conf_{i}")
        os.makedirs(conf_dir, exist_ok=True)

        old_inp_copy = os.path.join(conf_dir, "dftb_in_orig.hsd")
        if not os.path.exists(old_inp_copy):
            copy(inp_file, old_inp_copy)

        # Attempt to set the cell if 'cell' is truthy (not None/False) and valid
        if cell:
            try:
                atoms.set_cell(cell)
                atoms.set_pbc(True)
                logger.info(f"Set cell on Atoms for conf_{i} to {cell}, pbc=True.")
            except ValueError:
                # Typically arises if 'cell' isn't 3x3, 6-length, or 3-length
                logger.warning(f"Could not set cell {cell}; invalid shape? Skipping.")

        coord_file = os.path.join(conf_dir, "coord.gen")
        write(coord_file, atoms, format="gen")

        new_inp_file = os.path.join(conf_dir, "dftb_in.hsd")
        lines_new = [
            "Geometry = GenFormat {\n",
            f'  <<< "{os.path.basename(coord_file)}"\n',
            "}\n\n"
        ]
        for block_name, raw_block in other_blocks.items():
            lines_new.append(raw_block)
            if not raw_block.endswith("\n"):
                lines_new.append("\n")

        with open(new_inp_file, "w") as f:
            f.writelines(lines_new)

        #logger.info(
        #    f"[prep_dftbplus] Created '{new_inp_file}' (conf_{i}). "
        #    f"OutputPrefix='{proj_name}' (if Driver block present)."
        #)

def prep_mace(inp_file: str, run_type: str, atms_list: list, proj_name: str, model_path: str, pbc_cell=None):
    """Prepares the directories to run calculations with MACE.

    @param inp_file: MACE input YAML file specifying optimizer settings.
    @param run_type: Type of calculation. 'isolated', 'screening' or 'refinement'.
    @param atms_list: List of ase.Atoms objects to run the calculation on.
    @param proj_name: Name of the project.
    @param model_path: Path to the MACE model file.
    @param pbc_cell: Periodic boundary conditions (cell matrix) if applicable.
    """
    
    import os
    import yaml
    import numpy as np

    if not isinstance(inp_file, str):
        err_msg = "'inp_file' must be a string with the path of the MACE input YAML file."
        logger.error(err_msg)
        raise ValueError(err_msg)

    # Read YAML settings
    if not os.path.isfile(inp_file):
        err_msg = f"Error: MACE input file '{inp_file}' not found."
        logger.error(err_msg)
        raise FileNotFoundError(err_msg)

    with open(inp_file, "r") as yaml_file:
        mace_settings = yaml.safe_load(yaml_file)

    # Extract MACE parameters from YAML
    optimizer = mace_settings.get("optimizer", "BFGS")  # Default to BFGS
    fmax = mace_settings.get("fmax", 0.05)  # Default 0.05 eV/Å
    max_steps = mace_settings.get("max_steps", 500)  # Default 500 steps
    yaml_pbc = mace_settings.get("pbc", None)  # Read PBC setting from YAML

    logger.info(f"Preparing MACE optimization with {optimizer}, fmax={fmax}, max_steps={max_steps}")

    if not os.path.exists(run_type):
        os.mkdir(run_type)

    # Creating and setting up directories for every configuration
    for i, conf in enumerate(atms_list):
        subdir = f'{run_type}/conf_{i}/'
        os.mkdir(subdir)

        # Determine PBC: Use YAML if provided, otherwise use `pbc_cell`
        if yaml_pbc is not None:
            pbc = np.array(yaml_pbc)
        elif pbc_cell is False and np.linalg.det(conf.cell) != 0.0:
            pbc = conf.cell
        else:
            pbc = None

        # Apply PBC to structure if applicable
        if pbc is not None:
            conf.set_cell(pbc)
            conf.set_pbc(True)

        # Save the atomic structure as a .gen file
        coord_file = f"struct_{i}.gen"
        conf.write(os.path.join(subdir, coord_file), format="gen")

        # Generate `run_opt.py` for this configuration
        run_script = os.path.join(subdir, "run_opt.py")
        with open(run_script, "w") as f:
            f.write(f"""#!/usr/bin/env python3
import os
import time
import numpy as np
import torch
from ase.io import read, write
from ase.optimize import {optimizer}
from mace.calculators import MACECalculator

EV_TO_KCAL_MOL = 23.0605
MODEL_PATH = "{model_path}"
LOG_FILE = "opt_log.out"

# Ensure MODEL_PATH is valid
if MODEL_PATH is None or MODEL_PATH.lower() == "false":
    raise FileNotFoundError("Error: MODEL_PATH is not set correctly! Please provide a valid model file.")

# Load structure
atoms = read("{coord_file}")

#  Check SLURM-assigned GPU
slurm_gpu = os.getenv("CUDA_VISIBLE_DEVICES")
if slurm_gpu is not None and torch.cuda.is_available():
    device = f"cuda:{{slurm_gpu}}"
elif torch.cuda.is_available():
    device = "cuda"
else:
    device = "cpu"

print(f"Running on assigned device: {{device}}")

#  Flush GPU memory before starting
if "cuda" in device and torch.cuda.is_available():
    print("Flushing GPU memory before starting...")
    torch.cuda.empty_cache()

#  Load MACE model with correct device
calc = MACECalculator(model_paths=[MODEL_PATH], default_dtype="float32", device=device)
atoms.calc = calc

# Define optimizer
optimizer = {optimizer}(atoms, trajectory="opt_trajectory.traj")

# Optimization settings
fmax = {fmax}
max_steps = {max_steps}

# Open log file with header
with open(LOG_FILE, "w") as log_file:
    log_file.write("Step\\tEnergy (eV)\\tEnergy (kcal/mol)\\tMax Force (eV/Å)\\n")

step_numbers, energies_eV, energies_kcal_mol, max_forces = [], [], [], []

def log_optimization():
    step_number = len(step_numbers) + 1
    energy_eV = atoms.get_potential_energy()
    energy_kcal_mol = energy_eV * EV_TO_KCAL_MOL
    max_force = np.max(np.abs(atoms.get_forces()))

    step_numbers.append(step_number)
    energies_eV.append(energy_eV)
    energies_kcal_mol.append(energy_kcal_mol)
    max_forces.append(max_force)

    # Append properly formatted output to opt_log.out
    with open(LOG_FILE, "a") as log_file:
        log_file.write(f"{{step_number}}\\t{{energy_eV:.6f}}\\t{{energy_kcal_mol:.2f}}\\t{{max_force:.6f}}\\n")

    # Print formatted output for tracking
    print(f"Step {{step_number}}\\tEnergy = {{energy_eV:.6f}} eV\\t({{energy_kcal_mol:.2f}} kcal/mol)\\tMax Force = {{max_force:.6f}} eV/Å", flush=True)

optimizer.attach(log_optimization, interval=1)

# Run optimization
t0 = time.time()
converged = optimizer.run(fmax=fmax, steps=max_steps)  # Capture convergence status
t1 = time.time()

final_message = ""

if converged:
    final_message = "\\nOptimization finished successfully\\n"
else:
    final_message = "\\nWARNING: Optimization did NOT converge!\\n"

with open(LOG_FILE, "a") as log_file:
    log_file.write(final_message)

print(final_message.strip())

# Save the optimized structure
write("optimized_structure.xyz", atoms)
write("optimized_structure.gen", atoms, format="gen")
""")

        # Make run_opt.py executable
        os.chmod(run_script, 0o755)

    logger.info("MACE preparation completed.")


def get_jobs_status(job_ids, stat_cmd, stat_dict, missing_job_limit=3):
    """Returns a list of job statuses for a list of job IDs.

    @param job_ids: List of job IDs to check their status.
    @param stat_cmd: Command to check job status.
    @param stat_dict: Dictionary mapping (r, p, f) to expected scheduler outputs.
    @param missing_job_limit: Number of times a job can be missing before assuming it's finished.
    @return: List of statuses for every job.
    """
    from subprocess import PIPE, Popen

    status_list = []
    missing_jobs = {}
    missing_jobs_detected = False  # Track if we need to log a warning

    for job in job_ids:
        try:
            stat_msg = Popen(stat_cmd % job, shell=True, stdout=PIPE).communicate()[0].decode('utf-8').strip()

            if not stat_msg:  # Handle empty output (Job missing from queue)
                missing_jobs[job] = missing_jobs.get(job, 0) + 1
                if missing_jobs[job] >= missing_job_limit:
                    missing_jobs_detected = True  # Track the warning condition
                    status_list.append('f')  # Assume finished
                else:
                    status_list.append('unknown')

            elif stat_msg in stat_dict.values():  # Valid job status
                status_list.append(next(k for k, v in stat_dict.items() if v == stat_msg))

            else:
                logger.warning(f'Unrecognized job {job} status: "{stat_msg}"')
                status_list.append('unknown')

        except Exception as e:
            logger.error(f'Error checking status for job {job}: {e}')
            status_list.append('error')

    if missing_jobs_detected:
        logger.warning("Some jobs may have finished too quickly to be detected. Proceeding under this assumption.")

    return status_list


def submit_jobs(run_type, sub_cmd, sub_script, stat_cmd, stat_dict, max_jobs, name):
    """Submits jobs to a custom queuing system with the provided script (for CPU-based jobs).

    @param run_type: Type of calculation. 'isolated', 'screening', 'refinement'
    @param sub_cmd: Bash command used to submit jobs.
    @param sub_script: script for the job submission.
    @param stat_cmd: Bash command to check job status.
    @param stat_dict: Dictionary with pairs of job status: r, p, f (ie. running
        pending and finished) and the pattern it matches in the output of the
        stat_cmd.
    @param max_jobs: dict: Contains the maximum number of jobs to be both
        running, pending/queued and pending+running. When the relevant maximum
        is reached no jobs more are submitted.
    @param name: name of the project.
    """
    from shutil import copy
    from time import sleep
    from subprocess import PIPE, Popen
    from src.dockonsurf.utilities import _human_key

    subm_jobs = []
    init_dir = os.getcwd()

    for conf in sorted(os.listdir(run_type), key=_human_key):
        i = conf.split('_')[1]

        # Limit jobs based on max_jobs settings
        while True:
            job_statuses = get_jobs_status(subm_jobs, stat_cmd, stat_dict)
            running = job_statuses.count("r")
            pending = job_statuses.count("p")

            if running + pending < max_jobs['rp'] and running < max_jobs['r'] and pending < max_jobs['p']:
                break  # Safe to submit another job
            sleep(30)  # Wait before checking again

        # Copy and submit job
        copy(sub_script, f"{run_type}/{conf}")
        os.chdir(f"{run_type}/{conf}")
        job_name = f'{name[:5]}{run_type[:3].capitalize()}{i}'
        sub_order = sub_cmd % (job_name, sub_script)
        subm_msg = Popen(sub_order, shell=True, stdout=PIPE).communicate()[0]

        # Extract job ID
        job_id = None
        for word in subm_msg.decode("utf-8").split():
            try:
                job_id = int(word.replace('>', '').replace('<', ''))
                break
            except ValueError:
                continue

        if job_id is not None:
            subm_jobs.append(job_id)
        else:
            logger.error(f"Failed to retrieve job ID for {job_name}. Submission output: {subm_msg.decode('utf-8')}")

        sleep(1)  # Short delay before checking status
        status = get_jobs_status([job_id], stat_cmd, stat_dict)[0]
        if status == 'unknown':  # If missing, log it but don't spam warnings
            logger.info(f"Job {job_id} may have finished quickly after submission.")

        os.chdir(init_dir)

    logger.info('All jobs have been submitted, waiting for them to finish.')
    wait_time = 0
    while True:
        job_statuses = get_jobs_status(subm_jobs, stat_cmd, stat_dict)
        remaining_jobs = [j for j, s in zip(subm_jobs, job_statuses) if s not in ['f', 'unknown']]

        if not remaining_jobs:
            logger.info('All jobs finished successfully.')
            break  # All jobs finished

        logger.info(f"Waiting for jobs to finish. {len(remaining_jobs)} remaining: {remaining_jobs}")
        sleep(90)
        wait_time += 90

        if wait_time > 3600:  # If waiting for more than 1 hour, log a warning
            logger.warning(f"Jobs taking unusually long to finish. Current status: {job_statuses}")

def submit_jobs_mace(run_type, sub_cmd, sub_script, stat_cmd, stat_dict, max_jobs, name):
    """Submits MACE jobs to a custom queuing system with GPU-specific optimizations.

    @param run_type: Type of calculation. 'isolated', 'screening', 'refinement'
    @param sub_cmd: Bash command used to submit jobs.
    @param sub_script: script for the job submission.
    @param stat_cmd: Bash command to check job status.
    @param stat_dict: Dictionary with pairs of job status: r, p, f (ie. running
        pending and finished) and the pattern it matches in the output of the
        stat_cmd.
    @param max_jobs: dict: Contains the maximum number of jobs to be both
        running, pending/queued and pending+running. When the relevant maximum
        is reached no jobs more are submitted.
    @param name: name of the project.
    """
    from shutil import copy
    from time import sleep
    from subprocess import PIPE, Popen
    from src.dockonsurf.utilities import _human_key

    subm_jobs = []
    init_dir = os.getcwd()

    for conf in sorted(os.listdir(run_type), key=_human_key):
        i = conf.split('_')[1]

        # Limit GPU jobs based on max_jobs settings
        while True:
            job_statuses = get_jobs_status(subm_jobs, stat_cmd, stat_dict)
            running = job_statuses.count("r")
            pending = job_statuses.count("p")

            if running + pending < max_jobs['rp'] and running < max_jobs['r'] and pending < max_jobs['p']:
                break  # Safe to submit another job
            sleep(30)  # Wait before checking again

        # Introduce a fixed 15-second delay before submission to prevent GPU overload
        sleep(15)

        # Copy and submit job
        copy(sub_script, f"{run_type}/{conf}")
        os.chdir(f"{run_type}/{conf}")
        job_name = f'{name[:5]}{run_type[:3].capitalize()}{i}'
        sub_order = sub_cmd % (job_name, sub_script)
        subm_msg = Popen(sub_order, shell=True, stdout=PIPE).communicate()[0]

        # Extract job ID
        job_id = None
        for word in subm_msg.decode("utf-8").split():
            try:
                job_id = int(word.replace('>', '').replace('<', ''))
                break
            except ValueError:
                continue

        if job_id is not None:
            subm_jobs.append(job_id)
        else:
            logger.error(f"Failed to retrieve job ID for {job_name}. Submission output: {subm_msg.decode('utf-8')}")

        os.chdir(init_dir)

    # Wait for jobs to finish
    logger.info('All MACE jobs have been submitted, waiting for them to finish.')
    wait_time = 0
    while True:
        job_statuses = get_jobs_status(subm_jobs, stat_cmd, stat_dict)
        remaining_jobs = [j for j, s in zip(subm_jobs, job_statuses) if s not in ['f', 'unknown']]

        if not remaining_jobs:
            break  # All jobs finished

        logger.info(f"Waiting for GPU jobs to finish. {len(remaining_jobs)} remaining: {remaining_jobs}")
        sleep(90)
        wait_time += 90

        if wait_time > 3600:  # If waiting for more than 1 hour, log a warning
            logger.warning(f"GPU jobs taking unusually long to finish. Current status: {job_statuses}")

    logger.info('All MACE jobs have finished.')

def run_calc(run_type, inp_vars, atms_list):
    """Directs the calculation run/submission.

    @param run_type: Type of calculation. 'isolated', 'screening' or 'refinement'
    @param inp_vars: Calculation parameters from input file.
    @param atms_list: List of ase.Atoms objects containing the sets of atoms aimed to run the calculations of.
    """
    import os
    from src.dockonsurf.utilities import check_bak

    run_types = ['isolated', 'screening', 'refinement']
    if not isinstance(run_type, str) or run_type.lower() not in run_types:
        run_type_err = f"'run_type' must be one of the following: {run_types}"
        logger.error(run_type_err)
        raise ValueError(run_type_err)

    if inp_vars['batch_q_sys']:
        logger.info(f"Running {run_type} calculation with {inp_vars['code']} on {inp_vars['batch_q_sys']}.")
    else:
        logger.info(f"Doing a dry run of {run_type}.")

    if not os.path.exists(run_type):
        os.mkdir(run_type)

    input_files = {
        'isolated': 'isol_inp_file',
        'screening': 'screen_inp_file',
        'refinement': 'refine_inp_file',
    }

    # Select the correct preparation function
    if inp_vars['code'] == 'cp2k':
        prep_cp2k(inp_vars[input_files[run_type]], run_type, atms_list, inp_vars['project_name'])
    elif inp_vars['code'] == "vasp":
        prep_vasp(inp_vars[input_files[run_type]], run_type, atms_list, inp_vars['project_name'], inp_vars['pbc_cell'], inp_vars['potcar_dir'])
    elif inp_vars['code'] == "dftb+":
        prep_dftbplus(inp_vars[input_files[run_type]], run_type, atms_list, inp_vars['project_name'], inp_vars['pbc_cell'])
    elif inp_vars['code'] == 'mace':
        prep_mace(inp_vars[input_files[run_type]], run_type, atms_list, inp_vars['project_name'], inp_vars['model_mace'], inp_vars['pbc_cell'])
    else:
        err_msg = f"Preparation not implemented for '{inp_vars['code']}'."
        logger.error(err_msg)
        raise NotImplementedError(err_msg)

    # Define job status commands & dictionary mappings per queue system
    queue_systems = {
        "sge": {
            "stat_cmd": "qstat | grep %s | awk '{print $5}'",
            "stat_dict": {'r': 'r', 'p': 'qw', 'f': ''},
            "sub_cmd": 'qsub -N %s %s'
        },
        "slurm": {
            "stat_cmd": "squeue | grep %s | awk '{print $5}'",
            "stat_dict": {'r': 'R', 'p': 'PD', 'f': 'CD', 'c': 'CG', 'finishing': 'CG', 'unknown': ''},
            "sub_cmd": 'sbatch -J %s %s'
        },
        "lsf": {
            "stat_cmd": "bjobs -w | grep %s | awk '{print $3}'",
            "stat_dict": {'r': 'RUN', 'p': 'PEND', 'f': ''},
            "sub_cmd": 'bsub -J %s < %s'
        },
        "irene": {
            "stat_cmd": "ccc_mstat | grep %s | awk '{print $10}' | cut -c1",
            "stat_dict": {'r': 'R', 'p': 'P', 'f': ''},
            "sub_cmd": 'ccc_msub -r %s %s'
        }
    }

    # Get the correct queue settings
    queue_settings = queue_systems.get(inp_vars['batch_q_sys'])

    if queue_settings:
        stat_cmd = queue_settings["stat_cmd"]
        stat_dict = queue_settings["stat_dict"]
        sub_cmd = queue_settings["sub_cmd"]

        # **Use submit_jobs_mace for GPU-based MACE calculations**
        if inp_vars['code'] == 'mace':
            submit_jobs_mace(run_type, sub_cmd, inp_vars['subm_script'], stat_cmd, stat_dict, inp_vars['max_jobs'], inp_vars['project_name'])
        else:
            submit_jobs(run_type, sub_cmd, inp_vars['subm_script'], stat_cmd, stat_dict, inp_vars['max_jobs'], inp_vars['project_name'])

    elif inp_vars['batch_q_sys'] == 'local':
        pass  # TODO: Implement local execution
    elif not inp_vars['batch_q_sys']:
        pass  # Dry run, do nothing
    else:
        err_msg = f"Unknown batch queue system: {inp_vars['batch_q_sys']}."
        logger.error(err_msg)
        raise ValueError(err_msg)

