"""Module for the conversion between different kinds of atomic data.

functions:
confs_to_mol_list: Converts the conformers inside a rdkit.Mol object to a list
    of separate rdkit.Mol objects.
rdkit_mol_to_ase_atoms: Converts a rdkit.Mol object into ase.Atoms object.
add_special_atoms: Allows ase to use custom elements with symbols not in the
    periodic table.
adapt_format: Converts the coordinate files into a required library object type.
read_coords_cp2k: Reads the coordinates from a CP2K restart file and returns an
    ase.Atoms object.
read_coords_vasp: Reads the coordinates from VASP OUTCAR file and returns an
    ase.Atoms object.
read_energy_cp2k: Reads the CP2K out file and returns its final energy.
collect_confs: Reads the coordinates and energies of a list of finished
    calculations.
read_coords_dftb : Reads the DFTB out file and returns ase.Atoms object.
read_energy_dftb : Reads the DFT detailed.out file and retruns its final energy.
read_dftb_plus_hsd : Reads a DFTB+ HSD input file, returning ase.aToms object (from Geomtry block), cell, and the block name
"""

import logging
import io
import os
import numpy as np
import rdkit.Chem.AllChem as Chem
from ase import Atoms

logger = logging.getLogger('DockOnSurf')


def confs_to_mol_list(mol: Chem.rdchem.Mol, idx_lst=None):
    """Converts the conformers inside a rdkit mol object to a list of
    separate mol objects.

    @param mol: rdkit mol object containing at least one conformer.
    @param idx_lst: list of conformer indices to be considered. If not passed,
        all conformers are considered.
    @return: list of separate mol objects.
    """
    if idx_lst is None:
        idx_lst = list(range(mol.GetNumConformers()))
    return [Chem.MolFromMolBlock(
        Chem.MolToMolBlock(mol, confId=int(idx)).replace("3D", ""),
        removeHs=False) for idx in idx_lst]


def rdkit_mol_to_ase_atoms(mol: Chem.rdchem.Mol):
    """Converts a rdkit mol object into ase Atoms object.
    @param mol: rdkit mol object containing only one conformer.
    @return ase.Atoms: ase Atoms object with the same coordinates.
    """
    from ase import Atoms
    if mol.GetNumConformers() > 1:
        logger.warning('A mol object with multiple conformers is parsed, '
                       'converting to Atoms only the first conformer.')
    symbols = [atm.GetSymbol() for atm in mol.GetAtoms()]
    positions = mol.GetConformer(0).GetPositions()
    return Atoms(symbols=symbols, positions=positions)


def add_special_atoms(symbol_pairs):
    """Allows ase to use custom elements with symbols not in the periodic table.

    This function adds new chemical elements to be used by ase. Every new custom
    element must have a traditional (present in the periodic table) partner
    from which to obtain all its properties.
    @param symbol_pairs: List of tuples containing pairs of chemical symbols.
        Every tuple contains a pair of chemical symbols, the first label must be
        the label of the custom element and the second one the symbol of the
        reference one (traditional present on the periodic table).
    @return:
    """  # TODO Enable special atoms for rdkit
    import numpy as np
    from ase import data
    for i, pair in enumerate(symbol_pairs):
        data.chemical_symbols += [pair[0]]
        z_orig = data.atomic_numbers[pair[1]]
        orig_iupac_mass = data.atomic_masses_iupac2016[z_orig]
        orig_com_mass = data.atomic_masses_common[z_orig]
        data.atomic_numbers[pair[0]] = max(data.atomic_numbers.values()) + 1
        data.atomic_names += [pair[0]]
        data.atomic_masses_iupac2016 = np.append(data.atomic_masses_iupac2016,
                                                 orig_iupac_mass)
        data.atomic_masses = data.atomic_masses_iupac2016
        data.atomic_masses_common = np.append(data.atomic_masses_common,
                                              orig_com_mass)
        data.covalent_radii = np.append(data.covalent_radii,
                                        data.covalent_radii[z_orig])
        data.reference_states += [data.reference_states[z_orig]]
        # TODO Add vdw_radii, gsmm and aml (smaller length)


def adapt_format(requirement, coord_file, spec_atms=tuple()):
    """Converts the coordinate files into a required library object type.

    Depending on the library required to use and the file type, it converts the
    coordinate file into a library-workable object.
    @param requirement: str, the library for which the conversion should be
        made. Accepted values: 'ase', 'rdkit'.
    @param coord_file: str, path to the coordinates file aiming to convert.
        Accepted file formats: all file formats readable by ase.
    @param spec_atms: List of tuples containing pairs of new/traditional
        chemical symbols.
    @return: an object the required library can work with.
    """
    import ase.io
    from ase.io.formats import filetype

    from src.dockonsurf.utilities import try_command

    req_vals = ['rdkit', 'ase']
    lib_err = f"The conversion to the '{requirement}' library object type" \
              f" has not yet been implemented"
    conv_info = f"Converted {coord_file} to {requirement} object type"

    fil_type_err = f'The {filetype(coord_file)} file format is not supported'

    if requirement not in req_vals:
        logger.error(lib_err)
        raise NotImplementedError(lib_err)

    if requirement == 'rdkit':
        from src.dockonsurf.xyz2mol import xyz2mol
        if filetype(coord_file) == 'xyz':  # TODO Include detection of charges.
            ase_atms = ase.io.read(coord_file)
            atomic_nums = ase_atms.get_atomic_numbers().tolist()
            xyz_coordinates = ase_atms.positions.tolist()
            rd_mol_obj = xyz2mol(atomic_nums, xyz_coordinates)
            logger.debug(conv_info)
            return Chem.AddHs(rd_mol_obj)
        elif filetype(coord_file) == 'mol':
            logger.debug(conv_info)
            return Chem.AddHs(Chem.MolFromMolFile(coord_file, removeHs=False))
        else:
            ase_atms = try_command(ase.io.read,
                                   [(ase.io.formats.UnknownFileTypeError,
                                     fil_type_err)],
                                   coord_file)
            atomic_nums = ase_atms.get_atomic_numbers().tolist()
            xyz_coordinates = ase_atms.positions.tolist()
            return xyz2mol(atomic_nums, xyz_coordinates)

    if requirement == 'ase':
        add_special_atoms(spec_atms)
        if filetype(coord_file) == 'xyz':
            logger.debug(conv_info)
            return ase.io.read(coord_file)
        elif filetype(coord_file) == 'mol':
            logger.debug(conv_info)
            rd_mol = Chem.AddHs(Chem.MolFromMolFile(coord_file, removeHs=False))
            return rdkit_mol_to_ase_atoms(rd_mol)
        else:
            return try_command(ase.io.read,
                               [(ase.io.formats.UnknownFileTypeError,
                                 fil_type_err)],
                               coord_file)


def read_coords_cp2k(file, spec_atoms=tuple()):
    """Reads the coordinates from a CP2K restart file and returns an ase.Atoms
     object.

    @param file: The file to read containing the coordinates.
    @param spec_atoms: List of tuples containing the pairs of chemical symbols.
    @return: ase.Atoms object of the coordinates in the file.
    """
    import numpy as np
    from ase import Atoms
    from pycp2k import CP2K

    cp2k = CP2K()
    cp2k.parse(file)
    force_eval = cp2k.CP2K_INPUT.FORCE_EVAL_list[0]
    raw_coords = force_eval.SUBSYS.COORD.Default_keyword
    symbols = [atom.split()[0] for atom in raw_coords]
    positions = np.array([[float(coord) for coord in atom.split()[1:]]
                          for atom in raw_coords])
    if len(spec_atoms) > 0:
        add_special_atoms(spec_atoms)
    return Atoms(symbols=symbols, positions=positions)


def read_coords_vasp(file, spec_atoms=tuple()):
    """Reads the coordinates from VASP OUTCAR and returns an ase.Atoms object.

    @param file: The file to read containing the coordinates.
    @param spec_atoms: List of tuples containing the pairs of chemical symbols.
    @return: ase.Atoms object of the coordinates in the file.
    """
    import os
    import ase.io
    if not os.path.isfile(file):
        err_msg = f"File {file} not found."
        logger.error(err_msg)
        raise FileNotFoundError(err_msg)
    if len(spec_atoms) > 0:
        add_special_atoms(spec_atoms)
    return ase.io.read(file)


def read_energy_cp2k(file):
    """Reads the CP2K output file and returns its final energy.

    @param file: The file from which the energy should be read.
    @return: The last energy on the out file in eV.
    """
    out_fh = open(file, 'r')
    energy = None
    for line in out_fh:
        if "ENERGY| Total FORCE_EVAL ( QS ) energy" in line:
            energy = float(line.strip().split(':')[1]) * 27.2113845  # Ha to eV
    out_fh.close()
    return energy

def read_coords_dftb(file, spec_atoms=tuple()):
    """
    Reads the coordinates from a DFTB+ .xyz file and returns an ase.Atoms object.

    @param file: The .xyz file to read containing the coordinates.
    @param spec_atoms: List of tuples containing the pairs of chemical symbols.
    @return: ase.Atoms object of the coordinates in the file.
    """
    import os
    import ase.io

    if not os.path.isfile(file):
        err_msg = f"File {file} not found."
        logger.error(err_msg)
        raise FileNotFoundError(err_msg)

    if len(spec_atoms) > 0:
        add_special_atoms(spec_atoms)

    # Use ASE to read the .xyz file
    return ase.io.read(file)

def read_energy_dftb(file):
    """
    Reads the DFTB+ detailed.out file and returns the total energy in eV.

    @param file: The detailed.out file containing energy information.
    @return: The total energy in eV.
    """
    try:
        with open(file, 'r') as out_fh:
            total_energy = None
            for line in out_fh:
                if line.strip().startswith("Total energy:"):
                    # Parse the line to extract the energy in eV
                    total_energy = float(line.split()[-2])  # Second-to-last value is in eV
                    break
            if total_energy is None:
                raise ValueError("Total energy not found in the file.")
            return total_energy
    except FileNotFoundError:
        raise FileNotFoundError(f"File {file} not found.")
    except Exception as e:
        raise RuntimeError(f"Error reading energy from detailed.out: {e}")


def _parse_genformat_lines(gen_lines):
    """
    Given a list of lines from a GenFormat snippet (including possible cell lines),
    parse them into: atom_symbols, atom_positions, cell_matrix, pbc_tuple, fractional.
    """
    atom_symbols = []
    atom_positions = []
    cell_matrix = np.zeros((3, 3))
    fractional = False
    pbc_tuple = (False, False, False)

    idx = 0
    while idx < len(gen_lines) and (not gen_lines[idx].strip() or gen_lines[idx].strip().startswith('#')):
        idx += 1
    if idx >= len(gen_lines):
        return atom_symbols, atom_positions, cell_matrix, pbc_tuple, fractional

    header_line = gen_lines[idx].split('#', 1)[0].strip()
    header_parts = header_line.split()
    natoms = int(header_parts[0])
    if len(header_parts) > 1:
        mode = header_parts[1].upper()  # 'C','S','F','H', etc.
    else:
        mode = 'C'

    if mode == 'C':
        pbc_tuple = (False, False, False)
    elif mode == 'S':
        pbc_tuple = (True, True, True)
    elif mode == 'F':
        pbc_tuple = (True, True, True)
        fractional = True
    elif mode == 'H':
        pbc_tuple = (False, False, True)

    # Next line => species
    idx += 1
    while idx < len(gen_lines):
        if gen_lines[idx].strip() and not gen_lines[idx].strip().startswith('#'):
            break
        idx += 1
    if idx >= len(gen_lines):
        return atom_symbols, atom_positions, cell_matrix, pbc_tuple, fractional

    species_list = gen_lines[idx].strip().split()
    idx += 1

    # Read natoms lines (coordinates), skipping blanks/comments
    coord_count = 0
    while coord_count < natoms and idx < len(gen_lines):
        raw = gen_lines[idx].strip()
        idx += 1
        if not raw or raw.startswith('#'):
            continue
        parts = raw.split()
        # Format: <atom_number> <type_index> <x> <y> <z>
        tindex = int(parts[1]) - 1
        x, y, z = (float(v) for v in parts[2:5])
        atom_symbols.append(species_list[tindex])
        atom_positions.append([x, y, z])
        coord_count += 1

    post_coord_lines = []
    read_limit = 4
    while idx < len(gen_lines) and len(post_coord_lines) < read_limit:
        line = gen_lines[idx].strip()
        idx += 1
        if not line or line.startswith('#'):
            continue
        float_parts = line.split()
        try:
            _ = [float(v) for v in float_parts[:3]]
        except ValueError:
            break
        post_coord_lines.append(line)

    if len(post_coord_lines) == 4:
        for i_c in range(3):
            vec_str = post_coord_lines[i_c + 1].split()[:3]
            cell_matrix[i_c] = [float(v) for v in vec_str]
    elif len(post_coord_lines) == 3:
        for i_c in range(3):
            vec_str = post_coord_lines[i_c].split()[:3]
            cell_matrix[i_c] = [float(v) for v in vec_str]

    return atom_symbols, atom_positions, cell_matrix, pbc_tuple, fractional


def _parse_geometry_dftb_style(lines, basepath='.'):
    """
    Parses Geometry from either:
      - GENFormat inline or via <<< "filename"
      - or "explicit" DFTB+ style (TypeNames, LatticeVectors, etc.).
    Returns an ASE Atoms object.
    basepath: used to resolve relative filenames for <<< "filename".
    """
    atom_symbols = []
    atom_positions = []
    cell_matrix = np.zeros((3, 3))
    fractional = False
    pbc_tuple = (False, False, False)
    found_gen = False

    for i, line in enumerate(lines):
        l = line.strip()
        if not l or l.startswith('#'):
            continue

        # Check if we have "GenFormat" in line
        if 'genformat' in l.lower():
            found_gen = True
            block_lines = []
            brace_depth = 0
            if '{' in l:
                brace_depth = l.count('{') - l.count('}')
            block_lines.append(l)

            j = i + 1
            while j < len(lines) and brace_depth > 0:
                block_lines.append(lines[j])
                brace_depth += lines[j].count('{') - lines[j].count('}')
                j += 1

            inline_lines = []
            found_include = None

            # skip the opening line if it has "GenFormat {"
            if len(block_lines) > 0 and '{' in block_lines[0]:
                block_lines = block_lines[1:]
            # skip the closing brace if present
            if len(block_lines) > 0 and '}' in block_lines[-1]:
                block_lines = block_lines[:-1]

            # Now examine block_lines for a line containing <<< "filename"
            for blk_line in block_lines:
                l2 = blk_line.strip()
                if l2.startswith('<<<'):
                    import re
                    match = re.search(r'<<<\s*"([^"]+)"', l2)
                    if match:
                        found_include = match.group(1)  # filename
                else:
                    inline_lines.append(l2)

            if found_include:
                # read that file
                inc_path = os.path.join(basepath, found_include)
                with open(inc_path, 'r') as f:
                    inc_lines = f.readlines()
                # parse inc_lines
                (atom_symbols,
                 atom_positions,
                 cell_matrix,
                 pbc_tuple,
                 fractional) = _parse_genformat_lines(inc_lines)
            else:
                # parse the inline_lines as GenFormat
                (atom_symbols,
                 atom_positions,
                 cell_matrix,
                 pbc_tuple,
                 fractional) = _parse_genformat_lines(inline_lines)
            break  # done with geometry parse

    # If not GenFormat or not found, parse "explicit" style
    if not found_gen:
        tnames = []
        # find TypeNames, Periodic, LatticeVectors
        for i, line in enumerate(lines):
            s = line.strip()
            if not s or s.startswith('#'):
                continue
            if 'TypeNames' in s:
                tmp = s.replace('{',' ').replace('}',' ').split()
                extracted = [x.strip('"') for x in tmp if x.startswith('"') or x.endswith('"')]
                tnames.extend(extracted)
            elif 'Periodic' in s:
                if 'Yes' in s:
                    pbc_tuple = (True, True, True)
            elif 'LatticeVectors' in s:
                for c in range(3):
                    lv = lines[i + c + 1].strip()
                    while not lv or lv.startswith('#'):
                        c += 1
                        lv = lines[i + c + 1].strip()
                    cell_matrix[c] = [float(x) for x in lv.split()]

        # read TypesAndCoordinates
        reading_coords = False
        for line in lines:
            s = line.strip()
            if not s or s.startswith('#'):
                continue
            if 'TypesAndCoordinates' in s:
                reading_coords = True
                continue
            if reading_coords and '}' in s:
                break
            if reading_coords and 'TypesAndCoordinates' not in s:
                parts = s.split()
                if len(parts) >= 4:
                    idx = int(parts[0])
                    x,y,z = (float(k) for k in parts[1:4])
                    if 1 <= idx <= len(tnames):
                        sym = tnames[idx-1]
                    else:
                        sym = 'X'
                    atom_symbols.append(sym)
                    atom_positions.append([x, y, z])

    # Build ASE Atoms
    atom_positions = np.array(atom_positions, dtype=float)
    if fractional:
        atoms = Atoms(
            scaled_positions=atom_positions,
            symbols=atom_symbols,
            cell=cell_matrix,
            pbc=pbc_tuple
        )
    else:
        atoms = Atoms(
            positions=atom_positions,
            symbols=atom_symbols,
            cell=cell_matrix,
            pbc=pbc_tuple
        )
    return atoms


def read_dftb_plus_hsd(filename_or_fileobj):
    """
    Reads a DFTB+ HSD file, returning a dictionary with:
        - dftb_dict["atoms"]    -> ASE Atoms (parsed from 'Geometry')
        - dftb_dict["geometry"] -> dict with cell, positions, pbc, etc.
        - dftb_dict[BlockName]  -> raw multiline strings for other blocks
    Handles:
      - GenFormat inline
      - GenFormat with <<< "filename"
      - Explicit geometry
    """
    if hasattr(filename_or_fileobj, "read"):
        full_lines = filename_or_fileobj.readlines()
        basepath = '.'  # or some default
    else:
        import os
        basepath = os.path.dirname(os.path.abspath(filename_or_fileobj))
        with open(filename_or_fileobj, "r") as f:
            full_lines = f.readlines()

    lines = [ln.rstrip("\n") for ln in full_lines]

    # Parse geometry
    atoms = _parse_geometry_dftb_style(lines, basepath=basepath)
    geom = {}
    if len(atoms) > 0:
        geom["symbols"]   = atoms.get_chemical_symbols()
        geom["positions"] = atoms.get_positions().tolist()
        geom["pbc"]       = list(atoms.get_pbc())
        geom["cell"]      = atoms.get_cell().tolist()
    else:
        geom["symbols"]   = []
        geom["positions"] = []
        geom["pbc"]       = [False, False, False]
        geom["cell"]      = [[0.0, 0.0, 0.0],
                             [0.0, 0.0, 0.0],
                             [0.0, 0.0, 0.0]]

    # Collect other top-level blocks as raw
    blocks_raw = {}
    n = len(lines)
    i = 0
    brace_depth = 0
    current_block = None
    current_block_lines = []

    def finalize_block(bname, bcontent):
        blocks_raw[bname] = "\n".join(bcontent)

    while i < n:
        s = lines[i].strip()
        if not s or s.startswith('#'):
            i += 1
            continue

        if '=' in s and '{' in s:
            block_name = s.split('=', 1)[0].strip()
            current_block = block_name
            current_block_lines = []
            brace_depth = s.count('{') - s.count('}')
            current_block_lines.append(lines[i])
            i += 1

            while i < n and brace_depth > 0:
                cl = lines[i]
                current_block_lines.append(cl)
                brace_depth += cl.count('{') - cl.count('}')
                i += 1

            finalize_block(current_block, current_block_lines)
            current_block = None
            current_block_lines = []
        else:
            i += 1

    dftb_dict = {
        "atoms": atoms,
        "geometry": geom
    }
    for blk_name, raw_txt in blocks_raw.items():
        dftb_dict[blk_name] = raw_txt

    return dftb_dict

def collect_confs(dir_list, code, run_type, spec_atms=tuple()):
    """
    Reads the coordinates and energies of a list of finished calculations.

    Given a dockonsurf directory hierarchy: project/run_type/conf_X
    (run_type = ['isolated', 'screening', or 'refinement']), it reads the
    coordinates of each conf_X, assigns its total energy from the calculation,
    and assigns the conf_X label to track its origin. Finally, it returns the
    ase.Atoms object.

    @param dir_list: List of directories where to read the coords from.
    @param code: The code that produced the calculation results files.
    @param run_type: The type of calculation (and also the name of the folder)
        containing the calculation subdirectories.
    @param spec_atms: List of tuples containing pairs of new/traditional
        chemical symbols.
    @return: List of ase.Atoms objects.
    """
    from glob import glob
    import os
    from ase import Atoms
    from ase.io import read
    from src.dockonsurf.utilities import is_binary

    atoms_list = []

    for conf_dir in dir_list:
        conf_path = f"{run_type}/{conf_dir}/"
        if code == 'cp2k':
            ase_atms = read_coords_cp2k(glob(f"{conf_path}/*-1.restart")[0], spec_atms)
            # Assign energy
            for fil in os.listdir(conf_path):
                if is_binary(conf_path + fil):
                    continue
                conf_energy = read_energy_cp2k(conf_path + fil)
                if conf_energy is not None:
                    ase_atms.info["energy"] = conf_energy
                    break
            ase_atms.info[run_type[:3]] = conf_dir
            atoms_list.append(ase_atms)
        elif code == 'vasp':
            ase_atms = read_coords_vasp(f"{conf_path}/OUTCAR", spec_atms)
            ase_atms.info["energy"] = ase_atms.get_total_energy() * 27.2113845
            ase_atms.info[run_type[:3]] = conf_dir
            atoms_list.append(ase_atms)
        elif code == 'dftb+':
            xyz_file = glob(f"{conf_path}/*.xyz")
            if not xyz_file:
                raise FileNotFoundError(f"No .xyz file found in {conf_path}")
            ase_atms = read_coords_dftb(xyz_file[0], spec_atms)

            detailed_file = f"{conf_path}/detailed.out"
            if os.path.isfile(detailed_file):
                ase_atms.info["energy"] = read_energy_dftb(detailed_file)
            else:
                raise FileNotFoundError(f"detailed.out not found in {conf_path}")

            ase_atms.info[run_type[:3]] = conf_dir
            atoms_list.append(ase_atms)

        elif code == 'mace':
            # Read optimized structure
            gen_file = f"{conf_path}/optimized_structure.gen"
            xyz_file = f"{conf_path}/optimized_structure.xyz"
            opt_log = f"{conf_path}/opt_log.out"

            if os.path.isfile(gen_file):
                ase_atms = read(gen_file)
            elif os.path.isfile(xyz_file):
                ase_atms = read(xyz_file)
            else:
                raise FileNotFoundError(f"No optimized structure file found in {conf_path}")

            # Read final energy from opt_log.out
            if os.path.isfile(opt_log):
                try:
                    with open(opt_log, "r") as log_file:
                        lines = [line.strip() for line in log_file.readlines() if line.strip()]

                        # Ensure there are at least two lines (header + data)
                        if len(lines) < 2:
                            raise ValueError(f"opt_log.out in {conf_path} is incomplete.")

                        # Find the last valid energy line (ignoring final status message)
                        for line in reversed(lines):
                            parts = line.split()
                            if len(parts) >= 2 and parts[0].isdigit():  
                                final_energy = float(parts[1])  
                                ase_atms.info["energy"] = final_energy
                                break
                        else:
                            raise ValueError(f"No valid energy found in {opt_log}.")

                except Exception as e:
                    raise ValueError(f"Error reading energy from {opt_log}: {e}")
            else:
                raise FileNotFoundError(f"opt_log.out not found in {conf_path}")

            ase_atms.info[run_type[:3]] = conf_dir
            atoms_list.append(ase_atms)

        else:
            err_msg = f"Collect coordinates not implemented for '{code}'."
            logger.error(err_msg)
            raise NotImplementedError(err_msg)

    return atoms_list

