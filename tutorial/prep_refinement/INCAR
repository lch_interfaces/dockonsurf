# Algorithm and Precision
  PREC=Accurate       # Increased precision for refinement calculations.
  ALGO=Normal         # Normal electronic minimization algorithm for robustness.
  LREAL=Auto          # Automatic real-space projections based on system size.
  NELM=200            # Increased SCF cycle limit for better convergence.

# Functional and Dispersion/Dipole Correction
  GGA=PE              # PBE functional for exchange-correlation energy.
  IVDW=4              # DFT-D4 dispersion corrections for non-covalent interactions.
  ENCUT=400           # Increased plane-wave energy cutoff for higher accuracy.
  ENAUG=700           # Augmentation charge cutoff for PAW potentials, consistent with ENCUT.
  SYMPREC=1E-5        # Tighter symmetry precision for better handling of small distortions.

# Parallelization Parameters
  NCORE=8             # Number of cores per k-point for parallelization.
 # NPAR=8             # Optional: Parallelization over bands (adjust if needed).
 # NSIM=1             # Optional: Number of simultaneous k-point calculations.

# Ionic Relaxation Criterion
  IBRION=2            # Conjugate gradient algorithm for ionic relaxation.
  POTIM=0.5           # Ionic step size for relaxation, ensuring stability.
  NSW=120             # Increased number of ionic steps for convergence in refinement.

# Writing Flags
  LWAVE=F             # Write the WAVECAR file for wavefunction continuity.
  LCHARG=F            # Write the CHGCAR file to save the charge density.

# Convergence Criterion
  EDIFF=1E-06         # Tighter SCF energy convergence criterion for accuracy.
  EDIFFG=-0.01        # Stricter force convergence criterion for ionic relaxation.

# DOS-Related Parameters
  ISMEAR=-1           # Methfessel-Paxton smearing for isolated systems (e.g., molecules).
  SIGMA=0.05         # Small smearing width for accurate energy calculations.

