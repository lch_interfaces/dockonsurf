#!/bin/bash

# Directory to perform extraction
target_dir="../isolated"

# File to store the output
output_file="CPU.dat"

# Clear the file if it exists or create it if it doesn't
> "$output_file"

# Check if the target directory exists
if [[ -d "$target_dir" ]]; then
    # Loop through directories named conf_x in the target directory
    for dir in "$target_dir"/conf_*; do
        if [[ -d "$dir" ]]; then
            # Extract energy from OUTCAR in the directory
            energy=$(grep "Total CPU time used (sec):" "$dir/OUTCAR" | awk '{print $NF}')
            
            # Append directory name (relative) and energy to output file
            echo "$(basename "$dir") $energy" >> "$output_file"
        fi
    done
    echo "Extraction completed. Data saved in $output_file"
else
    echo "Target directory $target_dir does not exist."
fi

