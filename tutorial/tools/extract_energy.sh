#!/bin/bash

# Directory to perform extraction
target_dir="../isolated"

# File to store the output
output_file="E0.dat"

# Clear the file if it exists or create it if it doesn't
> "$output_file"

# Check if the target directory exists
if [[ -d "$target_dir" ]]; then
    # Loop through directories named conf_x in the target directory
    for dir in "$target_dir"/conf_*; do
        if [[ -d "$dir" ]]; then
            # Initialize e0 as missing value
            e0="NA"

            # Check if OSZICAR exists and is readable
            if [[ -f "$dir/OSZICAR" ]]; then
                # Extract the last line of OSZICAR
                last_line=$(tail -n 1 "$dir/OSZICAR")

                # Extract the E0 value (5th field, assuming consistent format)
                e0=$(echo "$last_line" | awk '{print $5}')
            fi

            # Append directory name (relative) and E0 to output file
            echo "$(basename "$dir") $e0" >> "$output_file"
        fi
    done
    echo "Extraction completed. Data saved in $output_file"
else
    echo "Target directory $target_dir does not exist."
fi

