#!/usr/bin/env python3

import numpy as np

# File containing the energy data
input_file = "E0.dat"

# Read the energy data
data = []
with open(input_file, "r") as file:
    for line in file:
        parts = line.strip().split()
        if len(parts) == 2:  # Ensure the line has two parts
            try:
                energy = float(parts[1]) * 27.2113845 # Convert energy to float and eV (from Hartree)
                data.append((parts[0], energy))  # Store (directory, energy) tuple
            except ValueError:
                continue  # Skip lines where the energy is not a valid number

# Ensure there is data to analyze
if len(data) == 0:
    print("No valid data found in the input file.")
    exit(1)

# Sort by energy
data.sort(key=lambda x: x[1])  # Sort by the second element (energy)

# Extract the lowest energy as the reference
reference_energy = data[0][1]

# Compute energy differences
differences = [(entry[0], entry[1], entry[1] - reference_energy) for entry in data]

# Get the five lowest differences (including the reference)
lowest_differences = differences[:20]

# Print the results
print("Five lowest energy differences (reference: {:.8f}):".format(reference_energy))
for dir_name, energy, diff in lowest_differences:
    print(f"{dir_name}: Energy = {energy:.8f}, Difference = {diff:.8f}")

