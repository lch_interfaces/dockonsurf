ase>=3.19.1
hdbscan~=0.8.26
matplotlib>=3.2.1
networkx>=2.4
numpy>=1.16.6
pycp2k~=0.2.2
pymatgen~=2020.11.11
python>=3.6
python-daemon~=2.2.4
rdkit>=2019.9.3
scikit-learn~=0.23.1
sphinx~=3.4.3
sphinx_rtd_theme~=0.4.3

